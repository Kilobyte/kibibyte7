Kibibyte is a (more or less) simple irc bot. Its aimed at general use and has a decent plugin system.

Required Dependencies:  
* PIrcBotx 0.9
* JOptionParser [https://bitbucket.org/Kilobyte/joptionparser](https://bitbucket.org/Kilobyte/joptionparser)
* Guava
* sl4j
