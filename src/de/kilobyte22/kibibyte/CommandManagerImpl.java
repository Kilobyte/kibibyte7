package de.kilobyte22.kibibyte;

import com.google.common.eventbus.Subscribe;
import de.kilobyte22.kibibyte.api.CommandSender;
import de.kilobyte22.kibibyte.api.NeedsPermission;
import de.kilobyte22.kibibyte.api.command.*;
import de.kilobyte22.kibibyte.api.core.Kibibyte;
import de.kilobyte22.kibibyte.api.event.CommandMessageEvent;
import de.kilobyte22.kibibyte.api.event.ExtendedSubscribe;
import de.kilobyte22.kibibyte.api.exception.CommandException;
import de.kilobyte22.kibibyte.api.exception.CommandNotFoundException;
import de.kilobyte22.kibibyte.api.exception.UsageException;
import de.kilobyte22.kibibyte.api.hook.CommandMessageHook;
import de.kilobyte22.kibibyte.api.plugin.PluginSystem;
import de.kilobyte22.kibibyte.api.registry.CommandRegistry;
import de.kilobyte22.kibibyte.command.CommandInfoImpl;
import de.kilobyte22.kibibyte.command.CommandParamsImpl;
import de.kilobyte22.kibibyte.core.Botmanager;
import de.kilobyte22.kibibyte.sender.ChannelCommandSender;
import de.kilobyte22.optionparse.OptionParseException;
import de.kilobyte22.optionparse.OptionUsageException;
import joptsimple.OptionParser;
import joptsimple.OptionSet;
import org.pircbotx.Channel;
import org.pircbotx.hooks.events.MessageEvent;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

public class CommandManagerImpl implements PluginCommandManager {

    Map<String, CommandInfo> commands = new HashMap<String, CommandInfo>();
    private static Logger logger = LoggerFactory.getLogger("CMDMGR");

    public CommandManagerImpl() {

    }

    @Override
    public Collection<CommandInfo> getCommands(PluginSystem.PluginType scope) {
        return Util.cloneCollection(commands.values()); //TODO: make scope aware
    }

    @Override
    public Collection<CommandInfo> getCommands() {
        return Util.cloneCollection(commands.values());
    }

    @Override
    public CommandInfo getCommand(String name, PluginSystem.PluginType scope) {
        CommandInfo info = commands.get(name);
        if (info == null)
            return null;
        if (scope.ordinal() >= info.getAnnotation().scope().ordinal())
            return info;
        else
            return null;
    }

    @Override
    public void register(Object o) {
        Class clazz = o.getClass();
        for (Method m : clazz.getMethods()) {
            if (m.isAnnotationPresent(Command.class)) {
                Class[] ptypes = m.getParameterTypes();
                if (!(ptypes.length == 1 && ptypes[0] == CommandParams.class)) {
                    logger.warn("Invalid Command Method definition: Invalid parameters - While registering {}#{}. Expected one Argument of Type {}. Ignoring.", clazz.getName(), m.getName(), CommandParams.class.getName());
                    continue;
                }
                Command annotation = m.getAnnotation(Command.class);
                commands.put(annotation.name(), new CommandInfoImpl(m, o, annotation, m.getAnnotation(NeedsPermission.class)));
            }
        }
    }

    @SuppressWarnings("deprecation")
    public void doCommand(String message, Channel channel, Kibibyte bot, CommandSender sender, boolean verbose) throws CommandNotFoundException {
        CommandMessageEvent e = new CommandMessageEvent(message, channel, sender);
        Botmanager.doHook(CommandMessageHook.class, e);
        message = e.message;
        String command = message.split(" ")[0];
        if (message.equals(command))
            message = "";
        else
            message = message.substring(command.length() + 1);
        CommandInfo info = CommandRegistry.getCommand(command, channel);
        if (info == null)
            throw new CommandNotFoundException(command);
        NeedsPermission perm = info.getPermission();
        if (perm != null && !sender.hasPermission(perm.value())) {
            sender.reportError("Access denied");
            return;
        }
        OptionSet options = null;
        String[] rawargs = de.kilobyte22.optionparse.OptionParser.splitArgs(message);
        if (!info.getAnnotation().disableParser()) {
            OptionParser parser = new OptionParser(info.getAnnotation().shortParserOptions());
            for (String s : info.getAnnotation().longParserOptions()) {
                parser.accepts(s);
            }
            options = parser.parse(rawargs);
        }
        try {
            info.getMethod().invoke(info.getHandler(), new CommandParamsImpl(sender, bot, options, message, channel, verbose));
        } catch (Exception e1) {
            boolean a = e1 instanceof InvocationTargetException;
            if (a && e1.getCause() instanceof UsageException) {
                sender.sendMessage("Usage: " + info.getAnnotation().usage());
            } else if (a && (e1.getCause() instanceof OptionParseException || e1.getCause() instanceof OptionUsageException)) {
                sender.reportError(e1.getCause().getMessage());
                sender.sendMessage("Usage: " + info.getAnnotation().usage());
            } else if (a && e1.getCause() instanceof CommandException) {
                sender.reportError("Error: " + e1.getCause().getMessage());
            } else {
                String c = "";
                if (channel == null)
                    c = "nochan";
                else
                    c = channel.getName();
                String botid = "";
                if (bot == null)
                    botid = "null";
                else
                    botid = bot.getId();
                logger.error("Error while calling command {} from {}@{}/{} with args '{}'", command, sender.getName(), botid, c, message);
                logger.error("Exception details", e1);
                sender.reportError("Oups, something went wrong there. Details were logged. Maybe inform one of the bots admins about this :)");
            }
        }
    }

    @Subscribe
    public void onMessage(MessageEvent<Kibibyte> event) {
        if (Botmanager.isIgnored(event.getUser())) return;
        Kibibyte bot = (Kibibyte) event.getBot();
        String msg = bot.stripPrefix(event.getMessage(), event.getChannel());
        if (msg == null) return;
        logger.info("{}@{} is running command {}", event.getUser().getNick(), event.getBot().getDisplayName(), msg);
        try {
            doCommand(msg, event.getChannel(), bot, new ChannelCommandSender(event.getUser(), event.getChannel(), bot), false);
        } catch (CommandNotFoundException e) {
            logger.info("Unknown command", e);
        }
    }

    public void unreg() {
        Botmanager.eventBus.register(this);
    }
}
