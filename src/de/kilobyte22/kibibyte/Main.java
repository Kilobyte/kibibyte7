package de.kilobyte22.kibibyte;

import de.kilobyte22.kibibyte.api.command.CommandManager;
import de.kilobyte22.kibibyte.api.registry.AuthRegistry;
import de.kilobyte22.kibibyte.api.registry.CommandRegistry;
import de.kilobyte22.kibibyte.api.registry.EventRegistry;
import de.kilobyte22.kibibyte.api.registry.PlaybackRegistry;
import de.kilobyte22.kibibyte.command.CoreCommands;
import de.kilobyte22.kibibyte.core.Botmanager;
import de.kilobyte22.kibibyte.core.Console;
import de.kilobyte22.kibibyte.event.filter.MessageFilter;
import de.kilobyte22.kibibyte.permission.NickservAccountHandler;
import de.kilobyte22.kibibyte.permission.YamlPermissionHandler;
import de.kilobyte22.kibibyte.playback.RamPlaybackHandler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.impl.SimpleLogger;

public class Main {

    public static void main(String[] args) {

        CommandManagerImpl manager = new CommandManagerImpl();
        Botmanager.eventBus.register(manager);
        manager.register(new CoreCommands());
        CommandRegistry.registerManager(manager);

        AuthRegistry.registerAccountHandler(new NickservAccountHandler());
        AuthRegistry.registerPermissionHandler(new YamlPermissionHandler());

        PlaybackRegistry.registerHandler(new RamPlaybackHandler());

        EventRegistry.registerFilter(new MessageFilter());

        new Console().start();

	    Botmanager.loadConfig();
        Botmanager.applyConfig();
    }
}
