package de.kilobyte22.kibibyte;

import java.util.regex.Matcher;

public class Match {
    private String[] groups;
    private String match;
    public Match(Matcher m) {
        match = m.group();
        groups = new String[m.groupCount()];
        for (int i = 0; i < m.groupCount(); i++) {
            groups[i] = m.group(i + 1);
        }
    }

    public String getMatch() {
        return match;
    }

    public String[] getGroups() {
        return groups;
    }
}
