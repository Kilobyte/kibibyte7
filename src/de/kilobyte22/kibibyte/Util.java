package de.kilobyte22.kibibyte;

import java.io.*;
import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Util {

    private static final Pattern unicodePattern = Pattern.compile("\\\\u([0-9a-fA-F]{4})");

    public static <T> Collection<T> cloneCollection(Collection<T> input) {
        Collection<T> ret = new LinkedList<T>();
        for (T e : input) {
            ret.add(e);
        }
        return ret;
    }

    public static void callDelayed(int delay, Runnable runnable) {
        new DelayThread(delay, runnable).start();
    }

    public static String readReader(Reader reader) throws IOException {
        BufferedReader r = new BufferedReader(reader);
        String ret = "";
        String tmp = null;
        boolean first = true;
        while ((tmp = r.readLine()) != null) {
            if (first)
                first = false;
            else
                ret += "\n";
            ret += tmp;
        }
        return ret;
    }

    public static String readFile(File file) throws IOException {
        return readReader(new FileReader(file));
    }

    public static List<Match> getAllMatches(String of, String on) {
        List<Match> allMatches = new ArrayList<Match>();
        Matcher m = Pattern.compile(of)
                .matcher(on);
        while (m.find()) {
            allMatches.add(new Match(m));
        }
        return allMatches;
    }

    public static String repeatString(String input, int times) {
        return new String(new char[times]).replace("\0", input);
    }

    public static String parseUnicode(CharSequence s) {
        Matcher m = unicodePattern.matcher(s);
        StringBuffer sb = new StringBuffer();
        while (m.find()) {
            int hex = Integer.valueOf(m.group(1),16);
            m.appendReplacement(sb, Character.toString((char)hex));
        }
        m.appendTail(sb);
        return sb.toString();
    }

    public static String prettifyInteger(int num) {
        String ret = "";
        while (num > 999) {
            int n = num % 1000;
            num -= n;
            num /= 1000;

            ret = "," + n + ret;
        }
        ret = num + ret;
        return ret;
    }

    public static String secToString(int input) {
        int secs = input % 60;
        input = (input - secs) / 60;

        int mins = input % 60;
        input = (input - mins) / 60;

        int hrs = input % 24;
        input = (input - hrs) / 24;

        int days = input % 7;
        input = (input - days) / 7;

        int weeks = input;

        if (weeks != 0)
            return String.format("%sw%sd", weeks, days);
        else if (days != 0)
            return String.format("%sd%sh", days, hrs);
        else if (hrs != 0)
            return String.format("%sh%sm", hrs, mins);
        else if (mins != 0)
            return String.format("%sm%ss", mins, secs);
        else
            return String.format("%ss", secs);
    }

    public static long unixtime() {
        return System.currentTimeMillis() / 1000L;
    }

    private static class DelayThread extends Thread {
        private final int delay;
        private final Runnable runnable;

        public DelayThread(int delay, Runnable runnable) {
            this.delay = delay;
            this.runnable = runnable;
        }

        @Override
        public void run() {
            try {
                Thread.sleep(delay);
                runnable.run();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
