package de.kilobyte22.kibibyte.api;

import org.pircbotx.Channel;
import org.pircbotx.User;

/**
 * This interface represents anything that could execute a Command.
 * It can be queried about just about anything on that Sender - like permissions or sending it a message
 */
public interface CommandSender {

    /**
     * Send a private message or notice to the User - depending on configuration
     * @param message The message to send, formatted like {@link java.lang.String#format} message
     * @param params The parameters for the {@link java.lang.String#format} call
     */
    void sendMessage(String message, String... params);

    /**
     * Returns the account used for permissions of that user
     * @return the account used for permissions of that user
     */
    String getAccount();

    /**
     * Checks if the sender has a certain permission
     * @param permission The permission to check for
     * @return {@code true} if the User has the permission
     */
    boolean hasPermission(String permission);

    /**
     * Returns the user connected to this sender, if applicable
     * @return the user connected to this sender or {@code null}
     */
    User getUser();

    /**
     *
     * @return
     */
    String getName();

    /**
     *
     * @param message
     * @param params
     */
    void reportError(String message, String... params);

    /**
     * Returns location. This can be the server name (display name) or 'Console' for example
     * @return
     */
    String getLocation();

    /**
     * Returns the ID of the network the Sender is on. 'CLI' is a reserved name for the console
     * @return the ID of the network the Sender is on. 'CLI' is a reserved name for the console
     */
    String networkID();

    /**
     * Checks if this command sender can see a certain {@link Channel}
     * @param channel the {@link Channel} to check for
     * @return {@code true} if the sender can see the {@link Channel}
     */
    boolean canSee(Channel channel);

    /**
     * Returns combo of accountname and network
     * @return combo of accountname and network
     */
    String getFullAccount();
}
