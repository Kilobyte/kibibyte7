package de.kilobyte22.kibibyte.api;

import org.pircbotx.Channel;

public interface CommandTarget {
    String getName();
    Channel getChannel();
}
