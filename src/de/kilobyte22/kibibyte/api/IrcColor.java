package de.kilobyte22.kibibyte.api;

public enum IrcColor {
    BOLD("\u0002"),
    COLOR("\u0003"),
    WHITE("00"),
    BLACK("01"),
    BLUE("02"),
    GREEN("03"),
    RED("04"),
    BROWN("05"),
    PURPLE("06"),
    ORANGE("07"),
    YELLOW("08"),
    LIME("09"),
    CYAN("10"),
    AQUA("11"),
    ROYAL("12"),
    PING("13"),
    GRAY("14"),
    LIGHT_GRAY("15");

    String code;
    IrcColor(String code) {
        this.code = code;
    }
    @Override
    public String toString() {
        return code;
    }
}
