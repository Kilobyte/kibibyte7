package de.kilobyte22.kibibyte.api.bufferplayback;


import org.pircbotx.hooks.events.ActionEvent;

public class ActionMessage extends TextMessage {

    String nick, message;

    public ActionMessage(String nick, String message) {
        this.nick = nick;
        this.message = message;
    }

    public ActionMessage(ActionEvent event) {
        this(event.getUser().getNick(), event.getMessage());
    }

    @Override
    public String formatLine() {
        return String.format("* %s %s", nick, message);
    }

    @Override
    public ActionMessage clone() {
        return new ActionMessage(nick, message);
    }

    @Override
    public String getMessage() {
        return message;
    }

    @Override
    public void setMessage(String message) {
        this.message = message;
    }
}
