package de.kilobyte22.kibibyte.api.bufferplayback;

import org.pircbotx.hooks.events.JoinEvent;

public class JoinMessage extends Message {
    private String nick;

    public JoinMessage(JoinEvent event) {
        this(event.getUser().getNick());
    }

    public JoinMessage(String nick) {
        this.nick = nick;
    }

    @Override
    public String formatLine() {
        return String.format("*** %s joined the channel", nick);
    }

    @Override
    public Message clone() {
        return new JoinMessage(nick);
    }
}
