package de.kilobyte22.kibibyte.api.bufferplayback;

import de.kilobyte22.kibibyte.Match;
import de.kilobyte22.kibibyte.Util;
import de.kilobyte22.kibibyte.api.configuration.ConfigList;
import de.kilobyte22.kibibyte.api.core.Kibibyte;
import de.kilobyte22.kibibyte.core.Botmanager;
import org.pircbotx.hooks.events.KickEvent;

import java.util.List;

public class KickMessage extends TextMessage {

    private String source, target, message, via;

    public KickMessage(KickEvent event) {
        String nick = event.getSource().getNick();
        String msg = event.getReason();
        String via = null;
        Kibibyte kibi = (Kibibyte) event.getBot();
        if (kibi.getServiceBots().contains(nick.toLowerCase()) && msg.matches("^\\(.*?\\) .*$")) {
            Match match = Util.getAllMatches("^\\(([^()]+)(?: \\([^)]*\\)\\) (.*))$", msg).get(0);
            via = nick;
            nick = match.getGroups()[0];
            msg = match.getGroups()[1];
        }
        this.source = nick;
        this.target = event.getRecipient().getNick();
        this.message = msg;
        this.via = via;
    }

    public KickMessage(String source, String target, String message, String via) {
        this.source = source;
        this.target = target;
        this.message = message;
        this.via = via;
    }

    @Override
    public String formatLine() {
        return String.format("*** %s kicked %s: %s", source, target + (via == null ? "" : " via " + via), message);
    }

    @Override
    public KickMessage clone() {
        return new KickMessage(source, target, message, via);
    }

    @Override
    public String getMessage() {
        return message;
    }

    @Override
    public void setMessage(String message) {
        this.message = message;
    }
}
