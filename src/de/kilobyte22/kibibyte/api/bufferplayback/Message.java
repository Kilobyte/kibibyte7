package de.kilobyte22.kibibyte.api.bufferplayback;

public abstract class Message {
    private Integer id;
    private boolean ignored = false;

    /**
     * Makes the message a String. frmat depends on Message type
     * @return The Message as String
     */
    public abstract String formatLine();

    /**
     * Creates an exact copy of the message
     * @return an exact copy of the message
     */
    public abstract Message clone();

    /**
     * Gets the internal ID of a message
     * Note that not all backends make use of this
     * @return The ID
     */
    public final Integer getID() {
        return id;
    }

    /**
     * Sets the ID. one-time use. Must be unique, Do not pass on when cloning a message!
     * @param i the ID
     * @throws UnsupportedOperationException if the ID was already set.
     */
    public final void setID(int i) throws UnsupportedOperationException {
        if (id != null) throw new UnsupportedOperationException("ID already set");
        id = i;
    }

    public void setIgnored(boolean state) {
        ignored = state;
    }

    public boolean isIgnored() {
        return ignored;
    }

    public String toString() {
        return String.format("{ignored: %s, id: %s, type: %s, data: %s}", ignored, (id != null ? id : "null"), this.getClass().getCanonicalName(), formatLine());
    }
}
