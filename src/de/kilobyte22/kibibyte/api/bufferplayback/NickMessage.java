package de.kilobyte22.kibibyte.api.bufferplayback;

import org.pircbotx.hooks.events.NickChangeEvent;

public class NickMessage extends Message {

    String oldnick, newnick;

    public NickMessage(NickChangeEvent event) {
        this(event.getOldNick(), event.getNewNick());
    }

    public NickMessage(String oldNick, String newNick) {
        oldnick = oldNick;
        newnick = newNick;
    }

    @Override
    public String formatLine() {
        return String.format("*** %s is now known as %s", oldnick, newnick);
    }

    @Override
    public NickMessage clone() {
        return new NickMessage(oldnick, newnick);
    }
}
