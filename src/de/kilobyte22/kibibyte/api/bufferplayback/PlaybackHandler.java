package de.kilobyte22.kibibyte.api.bufferplayback;

import org.pircbotx.Channel;

import java.util.Iterator;
import java.util.List;

public interface PlaybackHandler {
    /**
     * Gets Multiple messages of a channel
     * @param channel The channel
     * @param start The first message. note that negative numbers have same effect as positive bt they reverse the order and make it count from end of log rather than beginning
     * @param count The amount of messages
     * @return The List
     */
    public List<Message> getMessages(Channel channel, int start, int count);

    /**
     * Gets an infinite Iterator
     * @param start the first message to get, behaves like in {@link PlaybackHandler#getMessages(org.pircbotx.Channel, int, int)}
     * @return An iterator
     */
    public Iterator<Message> getMessageIterator(Channel channel, int start);

    /**
     * Adds a Message to the playback
     * The ID must either be not set or unique. Otherwise this call may throw an {@link IllegalAccessException}
     * @param channel the channel to add to
     * @param message The message to add
     * @throws IllegalArgumentException If ID is not unique
     */
    public void appendMessage(Channel channel, Message message) throws IllegalArgumentException;

    /**
     * The internal name of this handler
     * @return
     */
    public String getName();

    /**
     * Tells whether the Playback is written to disk or kept in RAM only. (temporary directory counts as RAM too)
     * @return {@code true}, if written to disk
     */
    public boolean isPersistant();

    /**
     * Tells the handler to prepare for a new channel
     * @param channel The channel
     */
    public void setupChannel(Channel channel);

    /**
     * Mainly intended for non-persistant handlers to clean up their data on that channel
     * @param channel The channel of question
     */
    public void cleanupChannel(Channel channel);

}
