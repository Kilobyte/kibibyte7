package de.kilobyte22.kibibyte.api.bufferplayback;

import org.pircbotx.hooks.events.MessageEvent;

public class RegularMessage extends TextMessage {

    String nick, message;

    private RegularMessage(String nick, String message) {
        this.nick = nick;
        this.message = message;
    }

    public RegularMessage(MessageEvent event) {
        this(event.getUser().getNick(), event.getMessage());
    }

    @Override
    public String formatLine() {
        return String.format("<%s> %s", nick, message);
    }

    @Override
    public RegularMessage clone() {
        return new RegularMessage(nick, message);
    }

    @Override
    public String getMessage() {
        return message;
    }

    @Override
    public void setMessage(String message) {
        this.message = message;
    }
}
