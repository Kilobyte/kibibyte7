package de.kilobyte22.kibibyte.api.bufferplayback;

/**
 * Text Messages are messages containing some kind of Text, like normal messages, kick messages or actions
 */

public abstract class TextMessage extends Message {
    /**
     * Returns the text of the message
     * @return the text of the message
     */
    public abstract String getMessage();

    /**
     * Sets the text of the message, should be used with care, as this may modify the existing message. If you want to be safe clone the message first
     * @param message
     */
    public abstract void setMessage(String message);

    public abstract TextMessage clone();
}
