package de.kilobyte22.kibibyte.api.command;

import de.kilobyte22.kibibyte.api.plugin.PluginSystem;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * This is used to annotate a command. Example:
 * <code>
 *     \<b>@Command(name = "test", usage = "&lt;param 1&gt; &lt;param 2&gt;", help = ")</b>
 *     public void test({@linkplain CommandParams} params) {
 *         params.sender.sendMessage("Hello world");
 *     }
 * </code>
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.METHOD)
public @interface Command {
    /**
     * The name of the command
     * @return The name of the command
     */
    String name();

    /**
     * The usage of the command.
     * <b>Do not include the name</b>
     * @return The usage of the command
     */
    String usage();

    /**
     * A short a help string
     * @return A short a help string
     */
    String help();

    @Deprecated
    String shortParserOptions() default "h*";

    @Deprecated
    String[] longParserOptions() default {"help"};

    boolean disableParser() default true;

    /**
     * The permission needed to execute the command. Leave out if no permission required. This should not be used for most things
     * @deprecated in favour of {@link de.kilobyte22.kibibyte.api.NeedsPermission}
     * @return The permission needed to execute the command - or "" if no permission required
     */
    @Deprecated
    String permission() default "";

    PluginSystem.PluginType scope() default PluginSystem.PluginType.GLOBAL;

    /**
     * Set to true for commands with a lot of output. Those are disalowed and can only be used from DCC or something like that
     * @return
     */
    boolean spammy() default false;
}
