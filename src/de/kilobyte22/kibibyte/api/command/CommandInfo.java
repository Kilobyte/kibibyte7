package de.kilobyte22.kibibyte.api.command;

import de.kilobyte22.kibibyte.api.NeedsPermission;

import java.lang.reflect.Method;

public interface CommandInfo {
    Method getMethod();
    Object getHandler();
    Command getAnnotation();
    NeedsPermission getPermission();
}
