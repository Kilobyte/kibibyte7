package de.kilobyte22.kibibyte.api.command;

import de.kilobyte22.kibibyte.api.plugin.PluginSystem;

import java.util.Collection;

public interface CommandManager {

    /**
     * Gets a list of all commands this manager is somehow responsible for
     * @return the list of commands wrapped in {@link CommandInfo}s
     */
    Collection<CommandInfo> getCommands(PluginSystem.PluginType scope);

    /**
     * Gets a certain command by its name
     * @param name The name of the command. Command names are always passed lowercase
     * @return The command, wrapped inside a {@link CommandInfo}
     */
    CommandInfo getCommand(String name, PluginSystem.PluginType scope);

    Collection<CommandInfo> getCommands();
}
