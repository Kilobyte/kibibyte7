package de.kilobyte22.kibibyte.api.command;

import de.kilobyte22.kibibyte.api.CommandSender;
import de.kilobyte22.kibibyte.api.core.Kibibyte;
import joptsimple.OptionParser;
import joptsimple.OptionSet;
import org.pircbotx.Channel;

public interface CommandParams {
    CommandSender getSender();
    Kibibyte getKibibyte();

    /**
     * The command Options
     * @deprecated in favour of JOptionParser. Please do the needed parsing yourself
     * @return The command Options
     */
    @Deprecated
    OptionSet getOptions();
    String[] getArgs();
    String getPlainArgsString();
    Channel getChannel();
    boolean isVerbose();
}
