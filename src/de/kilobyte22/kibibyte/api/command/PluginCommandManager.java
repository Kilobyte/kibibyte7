package de.kilobyte22.kibibyte.api.command;

public interface PluginCommandManager extends CommandManager {
    void register(Object o);
}
