package de.kilobyte22.kibibyte.api.configuration;

import org.yaml.snakeyaml.DumperOptions;
import org.yaml.snakeyaml.Yaml;

import java.io.*;
import java.util.Map;

public class ConfigFile extends ConfigMap {

    private File file;
    private static DumperOptions dopt = new DumperOptions();
    static {
        dopt.setDefaultFlowStyle(DumperOptions.FlowStyle.BLOCK);
        dopt.setIndent(4);
        //dopt.setDefaultScalarStyle(DumperOptions.ScalarStyle.SINGLE_QUOTED);
    }
    private static Yaml yaml = new Yaml(dopt);

    public ConfigFile() {}
    public ConfigFile(File file) {
        this.file = file;
    }

    public void load(InputStream stream) {
        setData((Map) yaml.load(stream));
    }

    public void load(String string) {
        setData((Map) yaml.load(string));
    }

    public void load() {
        try {
            if (!file.exists())
                file.createNewFile();
            setData((Map) yaml.load(new FileInputStream(file)));
        } catch (FileNotFoundException e) {
            logger.error("", e);
        } catch (IOException e) {
            logger.error("", e);
        }
    }

    public void save() {
        try {
            yaml.dump(toObject(), new FileWriter(file));
        } catch (IOException e) {
            logger.error("", e);
        }
    }

    public String makeString() {
        return yaml.dump(toObject());
    }

    public void toStream(OutputStream stream) {
        yaml.dump(toObject(), new OutputStreamWriter(stream));
    }

}
