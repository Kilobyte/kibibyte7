package de.kilobyte22.kibibyte.api.configuration;

import java.util.*;

public class ConfigList extends ConfigNode implements List<ConfigNode> {

    LinkedList<ConfigNode> nodes = new LinkedList<ConfigNode>();

    public ConfigList(List contents) {
        for (Object o : contents) {
            nodes.add(ConfigNode.toNode(o));
        }
    }

    public ConfigList() {

    }

    @Override
    public Object toObject() {
        LinkedList list = new LinkedList();
        for(ConfigNode node : nodes) {
            list.add(node.toObject());
        }
        return list;
    }

    @Override
    public Iterator<ConfigNode> iterator() {
        return nodes.iterator();
    }

    @Override
    public Object[] toArray() {
        return new Object[0];
    }

    @Override
    public <T> T[] toArray(T[] a) {
        return null;
    }

    public int size() {
        return nodes.size();
    }

    @Override
    public boolean isEmpty() {
        return nodes.isEmpty();
    }

    @Override
    public boolean contains(Object o) {
        return nodes.contains(o);
    }

    public boolean containsObject(Object o) {
        for (ConfigNode n : nodes)
            if (n.toObject().equals(o)) return true;
        return false;
    }

    public boolean add(ConfigNode node) {
        return nodes.add(node);
    }

    public ConfigList addChained(ConfigNode node) {
        add(node);
        return this;
    }

    public ConfigList addChained(Object o) {
        add(toNode(o));
        return this;
    }

    @Override
    public boolean remove(Object o) {
        return nodes.remove(o);
    }

    @Override
    public boolean containsAll(Collection<?> c) {
        return nodes.containsAll(c);
    }

    @Override
    public boolean addAll(Collection<? extends ConfigNode> c) {
        return nodes.addAll(c);
    }

    @Override
    public boolean addAll(int index, Collection<? extends ConfigNode> c) {
        return nodes.addAll(index, c);
    }

    @Override
    public boolean removeAll(Collection<?> c) {
        return nodes.removeAll(c);
    }

    @Override
    public boolean retainAll(Collection<?> c) {
        return nodes.retainAll(c);
    }

    @Override
    public void clear() {
        nodes.clear();
    }

    @Override
    public ConfigNode get(int index) {
        return nodes.get(index);
    }

    @Override
    public ConfigNode set(int index, ConfigNode element) {
        return nodes.set(index, element);
    }

    @Override
    public void add(int index, ConfigNode element) {
        nodes.add(index, element);
    }

    @Override
    public ConfigNode remove(int index) {
        return nodes.remove(index);
    }

    public void removeObject(Object o) {
        ConfigNode node = null;
        Iterator<ConfigNode> iterator = this.iterator();
        while (iterator.hasNext()) {
            node = iterator.next();
            if (node != null && node.toObject().equals(o)) {
                iterator.remove();
            }
        }

    }

    @Override
    public int indexOf(Object o) {
        return nodes.indexOf(o);
    }

    @Override
    public int lastIndexOf(Object o) {
        return nodes.lastIndexOf(o);
    }

    @Override
    public ListIterator<ConfigNode> listIterator() {
        return nodes.listIterator();
    }

    @Override
    public ListIterator<ConfigNode> listIterator(int index) {
        return nodes.listIterator(index);
    }

    @Override
    public List<ConfigNode> subList(int fromIndex, int toIndex) {
        return nodes.subList(fromIndex, toIndex);
    }

    public ConfigList add(String s) {
        nodes.add(new ConfigString(s));
        return this;
    }

    public ConfigMap getMap(int key) {
        ConfigNode node = nodes.get(key);
        if (node == null) {
            return null;
        } else if (node instanceof ConfigMap) {
            return (ConfigMap) node;
        } else {
            return null;
        }
    }

    public ConfigList getList(int key) {
        ConfigNode node = nodes.get(key);
        if (node == null) {
            return null;
        } else if (node instanceof ConfigList) {
            return (ConfigList) node;
        } else {
            return null;
        }
    }
}
