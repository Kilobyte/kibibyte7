package de.kilobyte22.kibibyte.api.configuration;

public class ConfigLong extends ConfigNode {

    private Long data;

    public ConfigLong(Long data) {
        this.data = data;
    }

    @Override
    public Long toObject() {
        return data;
    }
}
