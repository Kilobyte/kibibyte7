package de.kilobyte22.kibibyte.api.configuration;

import java.util.*;

public class ConfigMap extends ConfigNode implements Map<String, ConfigNode> {

    protected HashMap<String, ConfigNode> data = new HashMap<String, ConfigNode>();

    public ConfigMap() {

    }

    public ConfigMap(Map options) {
        setData(options);
    }

    protected void setData(Map options) {
        if (options == null) return;
        for (Object k : options.keySet()) {
            String s = (String) k;
            Object o = options.get(k);
            data.put(s, toNode(o));
        }
    }

    @Override
    public Object toObject() {
        HashMap map = new HashMap();
        for (String s : data.keySet()) {
            map.put(s, data.get(s).toObject());
        }
        return map;
    }

    public Integer getInt(String key, Integer def) {
        ConfigNode node = data.get(key);
        if (node == null && def != null) {
            ConfigInt val = new ConfigInt(def);
            data.put(key, val);
            return def;
        } else if (node instanceof ConfigInt) {
            return ((ConfigInt) node).toObject();
        } else if (node instanceof ConfigString) {
            String val = getString(key);
            try {
                Integer value = Integer.parseInt(val);
                data.put(key, new ConfigInt(value));
                return value;
            } catch (Exception ex) {
                return null;
            }
        } else {
            return null;
        }
    }

    public Long getLong(String key) {
        return getLong(key, null);
    }

    public Long getLong(String key, Long def) {
        ConfigNode node = data.get(key);
        if (node == null && def != null) {
            ConfigLong val = new ConfigLong(def);
            data.put(key, val);
            return def;
        } else if (node instanceof ConfigLong) {
            return ((ConfigLong) node).toObject();
        } else if (node instanceof ConfigInt) {
            return ((long) ((ConfigInt) node).toObject());
        } else if (node instanceof ConfigString) {
            String val = getString(key);
            try {
                Long value = Long.parseLong(val);
                data.put(key, new ConfigLong(value));
                return value;
            } catch (Exception ex) {
                return null;
            }
        } else {
            return null;
        }
    }

    public Integer getInt(String key) {
        return getInt(key, null);
    }

    public Boolean getBoolean(String key, Boolean def) {
        ConfigNode node = data.get(key);
        if (node == null && def != null) {
            ConfigBoolean val = new ConfigBoolean(def);
            data.put(key, val);
            return def;
        } else if (node instanceof ConfigBoolean) {
            return ((ConfigBoolean) node).toObject();
        } else if (node instanceof ConfigString) {
            String val = getString(key);
            if (val.equalsIgnoreCase("true") || val.equalsIgnoreCase("yes")) {
                data.put(key, new ConfigBoolean(true));
                return true;
            }
            if (val.equalsIgnoreCase("false") || val.equalsIgnoreCase("no")) {
                data.put(key, new ConfigBoolean(true));
                return false;
            }
            return null;
        } else {
            return null;
        }
    }

    public Boolean getBoolean(String key) {
        return getBoolean(key, null);
    }

    @Override
    public int size() {
        return data.size();
    }

    @Override
    public boolean isEmpty() {
        return data.isEmpty();
    }

    @Override
    public boolean containsKey(Object key) {
        return data.containsKey(key);
    }

    @Override
    public boolean containsValue(Object value) {
        return data.containsValue(value);
    }

    @Override
    public ConfigNode get(Object key) {
        return data.get(key);
    }

    @Override
    public ConfigNode put(String key, ConfigNode value) {
        return data.put(key, value);
    }

    public ConfigMap putChained(String key, ConfigNode value) {
        data.put(key, value);
        return this;
    }

    public ConfigMap put(String key, Object value) {
        data.put(key, toNode(value));
        return this;
    }

    public ConfigMap putChained(String key, Object value) {
        put(key, value);
        return this;
    }

    @Override
    public ConfigNode remove(Object key) {
        return data.remove(key);
    }

    @Override
    public void putAll(Map<? extends String, ? extends ConfigNode> m) {
        data.putAll(m);
    }

    @Override
    public void clear() {
        data.clear();
    }

    @Override
    public Set<String> keySet() {
        return data.keySet();
    }

    @Override
    public Collection<ConfigNode> values() {
        return data.values();
    }

    @Override
    public Set<Entry<String, ConfigNode>> entrySet() {
        return data.entrySet();
    }

    public ConfigMap getMap(String key) {
        ConfigNode node = data.get(key);
        if (node == null) {
            ConfigMap map = new ConfigMap();
            data.put(key, map);
            return map;
        } else if (node instanceof ConfigMap) {
            return (ConfigMap) node;
        } else {
            return null;
        }
    }

    public ConfigList getList(String key) {
        ConfigNode node = data.get(key);
        if (node == null) {
            ConfigList list = new ConfigList();
            data.put(key, list);
            return list;
        } else if (node instanceof ConfigList) {
            return (ConfigList) node;
        } else {
            return null;
        }
    }

    public String getString(String key, String def) {
        ConfigNode node = data.get(key);
        if (node == null) {
            ConfigString str = new ConfigString(def);
            data.put(key, str);
            return def;
        } else if (node instanceof ConfigString) {
            return ((ConfigString) node).get();
        } else {
            return null;
        }
    }

    public String getString(String key) {
        ConfigNode node = data.get(key);
        if (node == null) {
            return null;
        } else if (node instanceof ConfigString) {
            return ((ConfigString) node).get();
        } else {
            return null;
        }
    }

    public ConfigMap setBoolean(String name, Boolean value) {
        if (value == null)
            data.remove(name);
        else {
            setString(name, value ? "true" : "false");
        }
        return this;
    }

    public ConfigMap setString(String name, String value) {
        if (value == null)
            data.remove(name);
        else {
            data.put(name, new ConfigString(value));
        }
        return this;
    }

    public ConfigMap setInteger(String name, Integer value) {
        if (value == null)
            data.remove(name);
        else {
            data.put(name, new ConfigInt(value));
        }
        return this;
    }

    public ConfigMap makeList(String name) {
        data.put(name, new ConfigList());
        return this;
    }

    public ConfigMap makeMap(String name) {
        data.put(name, new ConfigMap());
        return this;
    }

    public ConfigMap shallowClone() {
        ConfigMap ret = new ConfigMap();
        ret.data = data;
        return ret;
    }
}
