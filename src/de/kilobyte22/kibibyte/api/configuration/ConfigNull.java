package de.kilobyte22.kibibyte.api.configuration;

public class ConfigNull extends ConfigNode {
    @Override
    public Object toObject() {
        return null;
    }
}
