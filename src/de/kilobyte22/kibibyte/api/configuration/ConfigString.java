package de.kilobyte22.kibibyte.api.configuration;

public class ConfigString extends ConfigNode {


    private final String data;

    public ConfigString(String data) {
        this.data = data;
    }

    @Override
    public String toString() {
        return data;
    }

    @Override
    public Object toObject() {
        return data;
    }

    public String get() {
        return data;
    }
}
