package de.kilobyte22.kibibyte.api.core;

import de.kilobyte22.kibibyte.api.command.PluginCommandManager;
import de.kilobyte22.kibibyte.api.event.EventBus;
import org.slf4j.Logger;

public interface BotAccess {

    EventBus getEventBus();
    Logger getLogger();
    PluginCommandManager getCommandManager();
}
