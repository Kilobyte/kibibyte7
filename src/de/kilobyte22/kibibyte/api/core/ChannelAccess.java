package de.kilobyte22.kibibyte.api.core;

import org.pircbotx.Channel;

public interface ChannelAccess extends BotAccess {
    Channel getChannel();
    Kibibyte getBot();
}
