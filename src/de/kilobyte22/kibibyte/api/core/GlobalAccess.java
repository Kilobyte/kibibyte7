package de.kilobyte22.kibibyte.api.core;

import de.kilobyte22.kibibyte.api.plugin.PluginSystem;

public interface GlobalAccess extends BotAccess {
    PluginSystem getPluginSystem();
}
