package de.kilobyte22.kibibyte.api.core;

import de.kilobyte22.kibibyte.api.configuration.ConfigMap;
import de.kilobyte22.kibibyte.api.permission.AccountHandler;
import de.kilobyte22.kibibyte.api.permission.PermissionHandler;
import org.pircbotx.Channel;
import org.pircbotx.PircBotX;
import org.pircbotx.User;

import javax.naming.OperationNotSupportedException;
import java.util.List;

public abstract class Kibibyte extends PircBotX {
    private String displayName;
    private boolean serviceBots;

    public abstract ConfigMap getConfig();
    public abstract String stripPrefix(String message, Channel channel);
    public abstract String[] getPrefixes();
    public abstract void doDefaultMessage(User user, String message);
    public abstract String getId();
    public abstract void requestCap(String... caps);
    public abstract Boolean hasCap(String name);
    public abstract String getAccount(User user);
    public abstract boolean hasPermission(User user, Channel channel, String permission);
    public abstract void setAccountHandler(AccountHandler accountHandler) throws OperationNotSupportedException;
    public abstract AccountHandler getAccountHandler();
    public abstract PermissionHandler getPermissionHandler();
    public abstract void setPermissionHandler(PermissionHandler handler);
    public abstract String getDisplayName();
    public abstract List<String> getServiceBots();
}
