package de.kilobyte22.kibibyte.api.core;

public interface SystemLogger {
    public void info(String message, String... args);
    public void warn(String message, String... args);
    public void error(String message, String... args);
    public void severe(String message, String... args);
}
