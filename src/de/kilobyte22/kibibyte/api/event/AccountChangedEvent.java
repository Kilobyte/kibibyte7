package de.kilobyte22.kibibyte.api.event;

import org.pircbotx.User;

public class AccountChangedEvent {
    private final User user;
    private final String account;

    public AccountChangedEvent(User user, String account) {
        this.user = user;
        this.account = account;
    }

    public User getUser() {
        return user;
    }

    public String getAccount() {
        return account;
    }
}
