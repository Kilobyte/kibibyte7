package de.kilobyte22.kibibyte.api.event;

import de.kilobyte22.kibibyte.core.Kibibyte;

public class CapHandlerRegistrationHook {

    public final Kibibyte bot;

    public CapHandlerRegistrationHook(Kibibyte bot) {
        this.bot = bot;
    }
}
