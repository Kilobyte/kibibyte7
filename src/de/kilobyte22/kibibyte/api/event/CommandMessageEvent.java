package de.kilobyte22.kibibyte.api.event;

import de.kilobyte22.kibibyte.api.CommandSender;
import de.kilobyte22.kibibyte.core.Kibibyte;
import org.pircbotx.Channel;
import org.pircbotx.User;
import org.pircbotx.hooks.events.MessageEvent;
import org.pircbotx.hooks.types.GenericMessageEvent;

public class CommandMessageEvent {
    public String message;
    public Channel channel;
    public CommandSender sender;
    public CommandMessageEvent(String msg, Channel channel, CommandSender sender) {
        message = msg;
        this.channel = channel;
        this.sender = sender;
    }
}
