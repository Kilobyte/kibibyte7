package de.kilobyte22.kibibyte.api.event;

import de.kilobyte22.kibibyte.core.Kibibyte;

public class ConnectionDoneEvent {
    public final Kibibyte bot;

    public ConnectionDoneEvent(Kibibyte bot) {
        this.bot = bot;
    }
}
