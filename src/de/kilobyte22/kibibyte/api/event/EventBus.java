package de.kilobyte22.kibibyte.api.event;

public interface EventBus {
    void post(Object event);
    void register(Object handler);
    void unregister(Object handler);
}
