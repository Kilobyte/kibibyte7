package de.kilobyte22.kibibyte.api.event;

public interface EventFilter {
    boolean canPass(Object event, String param);
    String getName();
}
