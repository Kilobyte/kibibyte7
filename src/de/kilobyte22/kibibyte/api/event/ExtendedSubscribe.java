package de.kilobyte22.kibibyte.api.event;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.METHOD)
public @interface ExtendedSubscribe {
    public String[] filters() default {};
    public FilterLinking filterLinking() default FilterLinking.OR;

    public enum FilterLinking {
        AND,
        OR
    }
}
