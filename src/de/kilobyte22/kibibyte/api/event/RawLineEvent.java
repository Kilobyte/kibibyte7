package de.kilobyte22.kibibyte.api.event;

public class RawLineEvent {
    private final String line;

    public RawLineEvent(String line) {
        this.line = line;
    }

    public String getLine() {
        return line;
    }
}
