package de.kilobyte22.kibibyte.api.event;

public class RawLineEvent2 {
    private final String line;

    public RawLineEvent2(String line) {
        this.line = line;
    }

    public String getLine() {
        return line;
    }
}
