package de.kilobyte22.kibibyte.api.event;

import de.kilobyte22.kibibyte.api.CommandSender;

public class RehashEvent {
    private final CommandSender sender;

    public RehashEvent(CommandSender sender) {

        this.sender = sender;
    }

    public CommandSender getSender() {
        return sender;
    }
}
