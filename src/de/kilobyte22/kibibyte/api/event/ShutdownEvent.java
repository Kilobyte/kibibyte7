package de.kilobyte22.kibibyte.api.event;

public class ShutdownEvent extends BotSavingEvent {
    private final String message;

    public ShutdownEvent(String message) {

        this.message = message;
    }

    public String getMessage() {
        return message;
    }
}
