package de.kilobyte22.kibibyte.api.exception;

public class CommandException extends RuntimeException {
    public CommandException(String message) {
        super(message);
    }
}
