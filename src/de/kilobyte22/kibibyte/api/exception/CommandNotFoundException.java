package de.kilobyte22.kibibyte.api.exception;

public class CommandNotFoundException extends RuntimeException {
    private final String command;

    public CommandNotFoundException(String command) {
        super("Did not find command " + command);
        this.command = command;
    }

    public String getCommand() {
        return command;
    }
}
