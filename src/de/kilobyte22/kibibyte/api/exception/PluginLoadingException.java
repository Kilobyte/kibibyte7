package de.kilobyte22.kibibyte.api.exception;

public class PluginLoadingException extends Exception {
    public PluginLoadingException(String message) {
        super(message);
    }
}
