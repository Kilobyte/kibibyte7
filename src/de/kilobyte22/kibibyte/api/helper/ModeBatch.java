package de.kilobyte22.kibibyte.api.helper;

import org.pircbotx.Channel;

import java.util.LinkedList;
import java.util.List;

public class ModeBatch {

    private final Channel channel;
    private List<ModeWithParam> modesWithParam = new LinkedList<>();
    private List<ModeWithoutParam> modesWithoutParam = new LinkedList<>();
    
    public ModeBatch(Channel channel) {

        this.channel = channel;
    }

    private class ModeWithParam extends ModeWithoutParam {
        public String param;
    }

    public ModeBatch change(char mode, boolean value) {
        ModeWithoutParam tmp = new ModeWithoutParam();
        tmp.mode = mode;
        tmp.state = value;
        modesWithoutParam.add(tmp);
        return this;
    }

    public ModeBatch change(char mode, boolean value, String param) {
        ModeWithParam tmp = new ModeWithParam();
        tmp.mode = mode;
        tmp.state = value;
        tmp.param = param;
        modesWithParam.add(tmp);
        return this;
    }

    public void execute() {
        List<ModeWithParam> tmp = new LinkedList<>();
    }

    private class ModeWithoutParam {
        public char mode;
        public boolean state;
    }
}
