package de.kilobyte22.kibibyte.api.interpreter;

public interface Interpreter {
    KibiValue eval(String code, KibiMap env, KibiMap args);
}
