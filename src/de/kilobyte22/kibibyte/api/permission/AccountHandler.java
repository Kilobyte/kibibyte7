package de.kilobyte22.kibibyte.api.permission;

import de.kilobyte22.kibibyte.api.configuration.ConfigMap;
import de.kilobyte22.kibibyte.api.core.Kibibyte;
import org.pircbotx.User;

import javax.naming.OperationNotSupportedException;

public interface AccountHandler {
    String getName();
    String getAccount(User user);
    boolean isCaching(Kibibyte bot);
    void setupNetwork(Kibibyte bot) throws OperationNotSupportedException;
    public void setConfig(Kibibyte bot, ConfigMap config);
}
