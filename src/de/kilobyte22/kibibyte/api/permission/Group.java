package de.kilobyte22.kibibyte.api.permission;

public interface Group {
    public Boolean hasPermission(String permission);
    public Group getParent();
    public void setParent(Group group);
    public void setPermission(String permission, Boolean value);
}
