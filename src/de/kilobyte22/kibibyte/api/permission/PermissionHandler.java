package de.kilobyte22.kibibyte.api.permission;

import de.kilobyte22.kibibyte.api.core.Kibibyte;
import org.pircbotx.Channel;

import java.util.List;

public interface PermissionHandler {

    String getName();

    //region Direct Permission

    /**
     * Checks if a user has a certain permission
     * @param network The network that user is on
     * @param account The users account name OR a special name. special names start with : (like :default which represents ANY user)
     * @param permission The permission to probe for
     * @return {@code true}, if the permission is granted, {@code false}, if it is denied and {@code null} if there is nothing specifically set
     */
    Boolean hasPermission(Kibibyte network, String account, String permission);

    /**
     * Sets a permission for a user
     * @param network The network the user is on
     * @param account The account of that user
     * @param permission the permission to modify
     * @param value The value to set to
     */
    void setPermission(Kibibyte network, String account, String permission, Boolean value);

    /**
     * Checks if the permission is a channel permission
     *
     * A channel permission is a permission which can be granted by channel staff and not just the bot admin.
     *
     * @param permission The exact permission
     * @return {@code true}, if this permission is a Channel permission
     */
    boolean isChannelPermission(String permission);

    /**
     * Returns a list of all channel permissions
     * @return the list of all channel permissions
     */
    List<String> getChannelPermissions();

    /**
     * Sets or Unsets the channel permission flag on a permission string
     * @param permission the permission string
     * @param value what value to set the flag to
     */
    void setChannelPermission(String permission, boolean value);
    //endregion

    //region Server Handling

    /**
     * Called when a Bot changes to a different permission handler
     * @param bot The Bot that changed
     */
    void removeHandledServer(Kibibyte bot);

    /**
     * Called when a Bot selects this permission handler
     * @param bot The bot that changed
     */
    void addHandledServer(Kibibyte bot);
    //endregion

    //// GROUPS

    //region Group Stuff

    /**
     * gets a list of groups the account is member in
     * @param network The network of that account
     * @param account The actual account
     * @return a list of all groups
     */
    List<Group> getGroups(Kibibyte network, String account);

    /**
     * Returns a list of all available Groups
     * @return a list of all available Groups
     */
    List<Group> getGroups();

    /**
     * Returns a reference to a named group
     * @param name the name of the group
     * @return a reference to a named group
     */
    Group getGroup(String name);

    /**
     * Gets all groups that are specific to a certain channel
     * @param channel The channel
     * @return The list
     */
    List<Group> getChannelGroups(Channel channel);

    /**
     * Gets all the channel specific groups a certain user is member in
     * @param channel The channel
     * @param account The account
     * @return The list
     */
    List<Group> getChannelGroups(Channel channel, String account);


    /**
     * Creates a new Group
     * @param name the name for the new group
     * @return an instance of that group
     */
    Group makeGroup(String name);

    /**
     * Creates a new channel group
     * @param name The name for that group
     * @param target the channel of that group
     * @return an instance of that group
     */
    Group makeGroup(String name, Channel target);


    /**
     * Deletes a group
     * @param name the name of that group
     */
    void removeGroup(String name);

    /**
     * Deletes a channel group
     * @param name The name of the group to delete
     * @param channel The connected channel
     */
    void removeGroup(String name, Channel channel);
    //endregion
}

