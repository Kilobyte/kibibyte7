package de.kilobyte22.kibibyte.api.plugin;

import de.kilobyte22.kibibyte.api.core.ChannelAccess;

public interface ChannelPlugin extends Plugin {
    void load(ChannelAccess channel);

    /**
     * Called upon permanent removal from a channel. plugin may clean up configs to remove that channel
     */
    void cleanup();
}
