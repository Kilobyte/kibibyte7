package de.kilobyte22.kibibyte.api.plugin;

import de.kilobyte22.kibibyte.api.core.GlobalAccess;

public interface GlobalPlugin extends Plugin {
    void load(GlobalAccess access);
}
