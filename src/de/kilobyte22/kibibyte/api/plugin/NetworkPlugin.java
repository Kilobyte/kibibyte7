package de.kilobyte22.kibibyte.api.plugin;

import de.kilobyte22.kibibyte.api.core.Kibibyte;

public interface NetworkPlugin extends Plugin {
    void load(Kibibyte bot);
}
