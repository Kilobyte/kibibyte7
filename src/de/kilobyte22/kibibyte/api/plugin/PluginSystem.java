package de.kilobyte22.kibibyte.api.plugin;

import de.kilobyte22.kibibyte.api.core.Kibibyte;
import de.kilobyte22.kibibyte.api.exception.PluginLoadingException;
import org.pircbotx.Channel;

import java.io.IOException;

public interface PluginSystem {

    void loadPlugin(String name) throws IOException, ClassNotFoundException, IllegalAccessException, InstantiationException;
    void loadPlugin(String name, String clazz) throws ClassNotFoundException, IllegalAccessException, InstantiationException;
    void enablePlugin(String name, Kibibyte kibibyte) throws PluginLoadingException;
    void enablePlugin(String name, Channel channel) throws PluginLoadingException;
    void unloadPlugin(String name);
    void disablePlugin(String name, Kibibyte kibibyte);
    boolean disablePlugin(String name, Channel channel, boolean permanent);
    PluginType getPluginType(String name);
    boolean pluginExists(String name);
    public boolean pluginLoadable(String name);


    enum  PluginType {
        GLOBAL,
        SERVER,
        CHANNEL
    }
}
