package de.kilobyte22.kibibyte.api.registry;


import de.kilobyte22.kibibyte.api.permission.AccountHandler;
import de.kilobyte22.kibibyte.api.permission.PermissionHandler;

import java.util.HashMap;
import java.util.Map;

public class AuthRegistry {

    private static final Map<String, AccountHandler> accountHandlers = new HashMap<>();
    private static final Map<String, PermissionHandler> permissionHandlers = new HashMap<>();

    public static void registerAccountHandler(AccountHandler accountHandler) {
        accountHandlers.put(accountHandler.getName(), accountHandler);
    }

    public static AccountHandler getAccountHandler(String name) {
        if (!accountHandlers.containsKey(name)) throw new IllegalArgumentException("Unknown Handler");
        return accountHandlers.get(name);
    }

    public static void registerPermissionHandler(PermissionHandler permissionHandler) {
        permissionHandlers.put(permissionHandler.getName(), permissionHandler);
    }

    public static PermissionHandler getPermissionHandler(String name) {
        if (!permissionHandlers.containsKey(name)) throw new IllegalArgumentException("Unknown Handler");
        return permissionHandlers.get(name);
    }
}
