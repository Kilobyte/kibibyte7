package de.kilobyte22.kibibyte.api.registry;

import de.kilobyte22.kibibyte.api.command.CommandInfo;
import de.kilobyte22.kibibyte.api.command.CommandManager;
import de.kilobyte22.kibibyte.api.plugin.PluginSystem;
import org.pircbotx.Channel;

import java.util.*;

public class CommandRegistry {

    private static List<CommandManager> handlers = new LinkedList<CommandManager>();
    private static Map<Channel, List<CommandManager>> channelHandlers = new WeakHashMap<>();

    public static CommandInfo getCommand(String command, Channel channel) {
       /*if (channel != null && channelHandlers.containsKey(channel)) {
            for (CommandManager h : channelHandlers.get(channel)) {
                CommandInfo i = h.getCommand(command);
                if (i != null) return i;
            }
        }*/
        for (Channel chan : channelHandlers.keySet()) {
            List<CommandManager> mgrs = channelHandlers.get(chan);
            PluginSystem.PluginType scope = (channel == null || channel != chan) ? PluginSystem.PluginType.GLOBAL : PluginSystem.PluginType.CHANNEL;
            for (CommandManager m : mgrs) {
                CommandInfo i = m.getCommand(command, scope);
                if (i != null) return i;
            }
        }

        PluginSystem.PluginType scope = channel == null ? PluginSystem.PluginType.GLOBAL : PluginSystem.PluginType.CHANNEL;
        for(CommandManager h : handlers) {
            CommandInfo i = h.getCommand(command, scope);
            if (i != null) return i;
        }
        return null;
    }

    public static void registerManager(CommandManager manager) {
        handlers.add(manager);
    }

    public static void registerManager(CommandManager manager, Channel channel) {
        if (!channelHandlers.containsKey(channel))
            channelHandlers.put(channel, new LinkedList<CommandManager>());
        channelHandlers.get(channel).add(manager);
    }

    public static void removeManager(CommandManager manager) {
        handlers.remove(manager);
    }

    public static void removeManager(CommandManager manager, Channel channel) {
        if (!channelHandlers.containsKey(channel)) return;
        channelHandlers.get(channel).remove(manager);
    }

    public static List<CommandInfo> getAllCommands() {
        List<CommandInfo> ret = new LinkedList<>();
        for (CommandManager m : handlers) {
            ret.addAll(m.getCommands());
        }
        return ret;
    }
}
