package de.kilobyte22.kibibyte.api.registry;

import de.kilobyte22.kibibyte.api.event.EventFilter;

import java.util.HashMap;
import java.util.Map;

public class EventRegistry {

    private static final Map<String, EventFilter> filters = new HashMap<String, EventFilter>();

    public static EventFilter getFilter(String name) {
        return filters.get(name);
    }

    public static void registerFilter(EventFilter filter) {
        filters.put(filter.getName(), filter);
    }
}
