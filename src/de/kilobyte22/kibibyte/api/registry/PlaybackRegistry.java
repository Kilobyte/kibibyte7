package de.kilobyte22.kibibyte.api.registry;

import de.kilobyte22.kibibyte.api.bufferplayback.PlaybackHandler;
import org.pircbotx.Channel;

import java.util.HashMap;
import java.util.Map;
import java.util.WeakHashMap;

public class PlaybackRegistry {
    private static Map<Channel, PlaybackHandler> channels = new WeakHashMap<>();
    private static Map<String, PlaybackHandler> handlers = new HashMap<>();

    public static void registerHandler(PlaybackHandler handler) throws IllegalArgumentException{
        String name = handler.getName();
        if (handlers.containsKey(name))
            throw new IllegalArgumentException("Name already occupied");
        handlers.put(name, handler);
    }

    public static void setChannelHandler(Channel channel, String handler) {
        PlaybackHandler h = handlers.get(handler);
        if (h == null)
            throw new IllegalArgumentException("Unknown Handler");
        if (channels.containsKey(channel))
            channels.get(channel).cleanupChannel(channel);
        channels.put(channel, h);
        h.setupChannel(channel);
    }

    public static PlaybackHandler getHandler(Channel channel) {
        return channels.get(channel);
    }
}
