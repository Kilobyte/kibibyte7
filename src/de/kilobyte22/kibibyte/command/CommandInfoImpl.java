package de.kilobyte22.kibibyte.command;

import com.sun.corba.se.impl.activation.CommandHandler;
import de.kilobyte22.kibibyte.api.NeedsPermission;
import de.kilobyte22.kibibyte.api.command.Command;
import de.kilobyte22.kibibyte.api.command.CommandInfo;

import java.lang.reflect.Method;

public class CommandInfoImpl implements CommandInfo {

    private final Method method;
    private final Object handler;
    private final Command annotation;
    private final NeedsPermission permission;

    public CommandInfoImpl(Method method, Object handler, Command annotation, NeedsPermission permission) {
        this.method = method;
        this.handler = handler;
        this.annotation = annotation;
        this.permission = permission;
    }

    @Override
    public Method getMethod() {
        return method;
    }

    @Override
    public Object getHandler() {
        return handler;
    }

    @Override
    public Command getAnnotation() {
        return annotation;
    }

    @Override
    public NeedsPermission getPermission() {
        return permission;
    }
}
