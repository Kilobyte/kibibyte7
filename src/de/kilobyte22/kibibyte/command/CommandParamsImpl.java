package de.kilobyte22.kibibyte.command;

import de.kilobyte22.kibibyte.api.CommandSender;
import de.kilobyte22.kibibyte.api.command.CommandParams;
import de.kilobyte22.kibibyte.api.core.Kibibyte;
import de.kilobyte22.optionparse.OptionParser;
import joptsimple.OptionSet;
import org.pircbotx.Channel;

public class CommandParamsImpl implements CommandParams {

    private final CommandSender sender;
    private final Kibibyte bot;
    private final OptionSet options;
    private final String plainArgs;
    private final Channel channel;
    private final boolean verbose;
    private final String[] args;

    public CommandParamsImpl(CommandSender sender, Kibibyte bot, OptionSet options, String plainArgs, Channel channel, boolean verbose) {
        this.sender = sender;
        this.bot = bot;
        this.options = options;
        this.plainArgs = plainArgs;
        this.channel = channel;
        this.verbose = verbose;
        this.args = OptionParser.splitArgs(plainArgs);
    }

    @Override
    public CommandSender getSender() {
        return sender;
    }

    @Override
    public Kibibyte getKibibyte() {
        return bot;
    }

    @Override
    public OptionSet getOptions() {
        return options;
    }

    @Override
    public String[] getArgs() {
        return args;
    }

    @Override
    public String getPlainArgsString() {
        return plainArgs;
    }

    @Override
    public Channel getChannel() {
        return channel;
    }

    @Override
    public boolean isVerbose() {
        return verbose;
    }
}
