package de.kilobyte22.kibibyte.command;

import de.kilobyte22.kibibyte.api.NeedsPermission;
import de.kilobyte22.kibibyte.api.command.Command;
import de.kilobyte22.kibibyte.api.command.CommandInfo;
import de.kilobyte22.kibibyte.api.command.CommandParams;
import de.kilobyte22.kibibyte.api.configuration.ConfigMap;
import de.kilobyte22.kibibyte.api.core.SystemLogger;
import de.kilobyte22.kibibyte.api.event.BotSavingEvent;
import de.kilobyte22.kibibyte.api.event.RehashEvent;
import de.kilobyte22.kibibyte.api.exception.CommandException;
import de.kilobyte22.kibibyte.api.exception.PluginLoadingException;
import de.kilobyte22.kibibyte.api.exception.UsageException;
import de.kilobyte22.kibibyte.api.plugin.PluginSystem;
import de.kilobyte22.kibibyte.api.registry.CommandRegistry;
import de.kilobyte22.kibibyte.core.Botmanager;
import de.kilobyte22.kibibyte.core.Kibibyte;
import de.kilobyte22.kibibyte.core.SyslogHandler;
import de.kilobyte22.kibibyte.core.SystemLoggerImpl;
import de.kilobyte22.kibibyte.misc.AsciiTable;
import de.kilobyte22.optionparse.Option;
import de.kilobyte22.optionparse.OptionParser;
import de.kilobyte22.optionparse.OptionSet;
import org.pircbotx.Channel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.List;

public class CoreCommands {
    private static final Logger logger = LoggerFactory.getLogger("CORE_CMDS");
    private static final SystemLogger syslog = new SystemLoggerImpl("core");

    @Command(name = "test", usage = "", help = "")
    public void testCommand(CommandParams params) {
        String acc = params.getSender().getAccount();
        params.getSender().sendMessage("yay. your account is %s", acc == null ? "none" : "'" + acc + "'");
    }

    @Command(name = "perm", usage = "(give|take|reset) <user> <permission>", help = "Gives or takes a users permissions")
    @NeedsPermission("core.admin.perm")
    public void perm(CommandParams params) {
        OptionSet options = new OptionParser().parse(params.getArgs());
        String act;
        String user = options.getNonOptionArgOrError(1);
        String what = options.getNonOptionArgOrError(0).toLowerCase();
        String perm = options.getNonOptionArgOrError(2);
        switch (what) {
            case "give":
                params.getKibibyte().getPermissionHandler().setPermission(params.getKibibyte(), user, perm, true);
                act = "gave";
                break;
            case "take":
                params.getKibibyte().getPermissionHandler().setPermission(params.getKibibyte(), user, perm, false);
                act = "took";
                break;
            case "reset":
                params.getKibibyte().getPermissionHandler().setPermission(params.getKibibyte(), user, perm, null);
                act = "resetted";
                break;
            default: throw new UsageException();
        }
        syslog.info("%s/%s[%s] %s user %s permission %s%s", params.getSender().getLocation(), params.getSender().getName(), params.getSender().getName(), act, user + (what.equals("reset") ? "'s" : ""), (what.equals("reset") ? "setting on " : ""), perm);
        params.getSender().sendMessage("%s user %s permission %s%s", act, user + (what.equals("reset") ? "'s" : ""), (what.equals("reset") ? "setting on " : ""), perm);
    }

    @Command(name = "load", usage = "[-c class] <name>", help = "Loads a plugin", shortParserOptions = "c:")
    @NeedsPermission("core.admin.load")
    public void load(CommandParams params) {
        OptionParser parser = new OptionParser();
        Option classOption = parser.addOption("class").alias('c').withArgument().help("The class for the plugin");
        OptionSet options = parser.parse(params.getArgs());
        try {
            String plugin = options.getNonOptionArgOrError(0);
            if (Botmanager.pluginSystem.pluginExists(plugin))
                throw new CommandException("Plugin already loaded");
            if (options.isSet(classOption)) {
                String klass = options.getValueFor(classOption);
                Botmanager.pluginSystem.loadPlugin(plugin, klass);
                plugin += "@" + klass;
            } else {
                if (!Botmanager.pluginSystem.pluginLoadable(plugin))
                    throw new CommandException("Plugin does not exist");
                Botmanager.pluginSystem.loadPlugin(plugin);
            }
            params.getSender().sendMessage("Done");
            syslog.info("%s/%s[%s] loaded plugin %s", params.getSender().getLocation(), params.getSender().getName(), params.getSender().getAccount(), plugin);
        } catch (ClassNotFoundException | IllegalAccessException | InstantiationException | IOException e) {
            logger.error("Error while loading plugin: ", e);
            throw new CommandException("Could not load plugin");
        }
    }

    @Command(name = "enable", usage = "<plugin>", help = "Enables a plugin for this channel or server")
    @NeedsPermission("core.admin.enable")
    public void enable(CommandParams params) {

        OptionSet options = new OptionParser().parse(params.getArgs());
        String name = options.getNonOptionArgOrError(0);

        if (!Botmanager.pluginSystem.pluginExists(name))
            throw new CommandException("Unknown plugin");
        PluginSystem.PluginType type = Botmanager.pluginSystem.getPluginType(name);
        if (type == PluginSystem.PluginType.GLOBAL)
            throw new CommandException("This plugin is a global plugin");
        if (type == PluginSystem.PluginType.CHANNEL && params.getChannel() == null)
            throw new CommandException("This plugin must be loaded from a channel");
        if ((type == PluginSystem.PluginType.CHANNEL && !params.getSender().hasPermission("core.admin.enable.channel")) ||
                (type == PluginSystem.PluginType.SERVER && !params.getSender().hasPermission("core.admin.enable.server")))
            throw new CommandException("You don't have permission to load this type of plugin");
        try {
            if (type == PluginSystem.PluginType.CHANNEL)
                Botmanager.pluginSystem.enablePlugin(name, params.getChannel());
            else
                Botmanager.pluginSystem.enablePlugin(name, params.getKibibyte());
        } catch (PluginLoadingException e) {
            throw new CommandException(e.getMessage());
        }
        params.getSender().sendMessage("Done.");
    }

    @Command(name = "disable", usage = "<plugin>", help = "Disables a plugin for this channel or server")
    @NeedsPermission("core.admin.disable")
    public void disable(CommandParams params) {

        OptionSet options = new OptionParser().parse(params.getArgs());
        String name = options.getNonOptionArgOrError(0);

        if (!Botmanager.pluginSystem.pluginExists(name))
            throw new CommandException("Unknown plugin");
        PluginSystem.PluginType type = Botmanager.pluginSystem.getPluginType(name);
        if (type == PluginSystem.PluginType.GLOBAL)
            throw new CommandException("This plugin is a global plugin");
        if (type == PluginSystem.PluginType.CHANNEL && params.getChannel() == null)
            throw new CommandException("This plugin must be unloaded from a channel");
        if ((type == PluginSystem.PluginType.CHANNEL && !params.getSender().hasPermission("core.admin.disable.channel")) ||
                (type == PluginSystem.PluginType.SERVER && !params.getSender().hasPermission("core.admin.disable.server")))
            throw new CommandException("You don't have permission to unload this type of plugin");

        if (type == PluginSystem.PluginType.CHANNEL)
            if (!Botmanager.pluginSystem.disablePlugin(name, params.getChannel(), true))
                throw new CommandException("Plugin not loaded");
        else
            Botmanager.pluginSystem.disablePlugin(name, params.getKibibyte());

        params.getSender().sendMessage("Done.");
    }

    @Command(name = "exit", usage = "<message>", help = "shuts down the bot")
    @NeedsPermission("core.admin.exit")
    public void shutdown(CommandParams params) {
        String message = params.getPlainArgsString();
        syslog.info("%s/%s[%s] is shutting down the bot with message %s", params.getSender().getLocation(), params.getSender().getName(), params.getSender().getAccount(), message);
        Botmanager.shutdown(String.format("[%s@%s][exit] %s", params.getSender().getName(), params.getSender().getLocation(), message), false);
    }

    @Command(name = "restart", usage = "<message>", help = "restarts the bot")
    @NeedsPermission("core.admin.restart")
    public void restart(CommandParams params) {
        if (!Botmanager.canRestart())
            throw new CommandException("Restart Impossible. Make sure to launch through a launch script with proper settings.");
        String message = params.getPlainArgsString();
        if (params.getKibibyte() != null) {
            ConfigMap restartMap = params.getKibibyte().getConfig().getMap("restart-info");
            restartMap
                    .putChained("issuer", params.getSender().getName())
                    .putChained("timestamp", System.currentTimeMillis());
            Botmanager.config.save();
        }
        syslog.info("%s/%s[%s] is restarting the bot with message %s", params.getSender().getLocation(), params.getSender().getName(), params.getSender().getAccount(), message);
        Botmanager.shutdown(String.format("[%s@%s][restart] %s", params.getSender().getName(), params.getSender().getLocation(), message), true);
    }

    @Command(name = "chanperm", usage = "[(give|take|reset) <user> <permission>]", help = "Sets a channel permission. leave out to list available permissions")
    @NeedsPermission("core.chanadm.chanperm")
    public void chanperm(CommandParams params) {
        OptionSet options = new OptionParser().parse(params.getArgs());

        if (options.nonOptionCount() == 0) {
            List<String> perms = params.getKibibyte().getPermissionHandler().getChannelPermissions();
            String tmp = "";
            for (String perm : perms) {
                if (tmp.length() != 0)
                    tmp += ", ";
                tmp += perm;
            }
            params.getSender().sendMessage("Available permissions: %s", tmp);
            return;
        }
        if (options.nonOptionCount() < 3)
            throw new UsageException();

        // TODO: Fix

        if (!params.getKibibyte().getPermissionHandler().isChannelPermission(options.getNonOptionArg(2)))
            throw new CommandException("You may not set this permission.");
        String act;
        String user = options.getNonOptionArg(1);
        String what = options.getNonOptionArg(0).toLowerCase();
        String perm = options.getNonOptionArg(2);
        switch (what) {
            case "give":
                params.getKibibyte().getPermissionHandler().setPermission(params.getKibibyte(), user, perm, true);
                act = "gave";
                break;
            case "take":
                params.getKibibyte().getPermissionHandler().setPermission(params.getKibibyte(), user, perm, false);
                act = "took";
                break;
            case "reset":
                params.getKibibyte().getPermissionHandler().setPermission(params.getKibibyte(), user, perm, null);
                act = "resetted";
                break;
            default: throw new UsageException();
        }
        params.getSender().sendMessage("%s user %s permission %s%s", act, user + (what.equals("reset") ? "'s" : ""), (what.equals("reset") ? "setting on " : ""), perm);

    }

    @Command(name = "rehash", usage = "", help = "Reloads the bots config from disk")
    @NeedsPermission("core.admin.rehash")
    public void rehash(CommandParams params) {
        syslog.info("%s/%s[%s] is rehashing the configuration", params.getSender().getLocation(), params.getSender().getName(), params.getSender().getAccount());
        Botmanager.rehash(params.getSender());
    }

    @Command(name = "ignore", usage = "(add|del|list) [[a:]<user>]", help = "Manages ignores")
    @NeedsPermission("core.admin.ignore")
    public void ignore(CommandParams params) {
        OptionSet options = new OptionParser().parse(params.getArgs());
        // if (args.size() < 1) throw new CommandException("Insufficiant arguments");
        switch (options.getNonOptionArgOrError(0)) {
            case "list":
                params.getSender().sendMessage("not yet implemented :(");
                break;
            case "add":
                String user = options.getNonOptionArgOrError(1);
                boolean acc = false;
                if (user.startsWith("a:")) {
                    acc = true;
                    user = user.substring(2);
                }
                try {
                    Botmanager.ignore(params.getKibibyte().getUser(user), acc);
                } catch (IllegalArgumentException ex) {
                    throw new CommandException(ex.getMessage());
                }
                syslog.info("%s/%s[%s] added %s %s to the ignore list", params.getSender().getLocation(), params.getSender().getName(), params.getSender().getAccount(), acc ? "account" : "user", user);
                break;
            case "del":
                String user2 = options.getNonOptionArgOrError(1);
                boolean acc2 = false;
                if (user2.startsWith("a:")) {
                    acc2 = true;
                    user2 = user2.substring(2);
                }
                syslog.info("%s/%s[%s] removed %s %s from the ignore list", params.getSender().getLocation(), params.getSender().getName(), params.getSender().getAccount(), acc2 ? "account" : "user", user2);
                Botmanager.unignore(params.getKibibyte().getUser(user2), acc2);
                break;
        }
    }

    @Command(name = "help", usage = "[<topic>]", help = "Outputs some help")
    public void help(CommandParams params) {
        OptionSet options = new OptionParser().parse(params.getArgs());
        if (options.nonOptionCount() == 0) {
            // list commands

            List<CommandInfo> infos = CommandRegistry.getAllCommands();



            if (params.isVerbose()) {

                AsciiTable table = new AsciiTable("Command", "Permission", "Description");

                for (CommandInfo i : infos) {
                    String name = i.getAnnotation().name() + " " + i.getAnnotation().usage();
                    NeedsPermission perm = i.getPermission();
                    String p;
                    if (perm == null)
                        p = "<none>";
                    else {
                        p = perm.value();
                        if (!params.getSender().hasPermission(p))
                            continue;
                    }
                    String desc = i.getAnnotation().help();
                    table.addRow(name, p, desc);
                }

                params.getSender().sendMessage(table.render());
            } else {
                String list = "";
                for (CommandInfo i : infos) {

                    NeedsPermission perm = i.getPermission();
                    if (perm != null && !params.getSender().hasPermission(perm.value()))
                        continue;

                    if (list.length() > 0) list += ", ";
                    list += i.getAnnotation().name();
                }

                params.getSender().sendMessage("Avaliable commands: " + list);
            }
        }
    }

    @Command(name = "broadcast", usage = "<message>", help = "Sends a message to all connected networks in all channels")
    @NeedsPermission("core.admin.broadcast")
    public void broadcast(CommandParams params) {
        String msg = String.format("[%s@%s] %s", params.getSender().getName(), params.getSender().getLocation(), params.getPlainArgsString());
        int c = 0;
        for (Kibibyte bot : Botmanager.getAllBots().values()) {
            for (Channel channel : bot.getChannels()) {
                bot.sendMessage(channel, msg);
                c++;
            }
        }
        params.getSender().sendMessage("Successfully broadcasted to %s channels", String.valueOf(c));
    }

    @Command(name = "flushbot", usage = "", help = "Emits a BotSavingEvent causing all delayed writes to be done")
    @NeedsPermission("core.admin.flushbot")
    public void flushbot(CommandParams params) {
        Botmanager.eventBus.post(new BotSavingEvent());
        params.getSender().sendMessage("Done.");
    }
}
