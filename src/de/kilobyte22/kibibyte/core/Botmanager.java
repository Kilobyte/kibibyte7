package de.kilobyte22.kibibyte.core;

import de.kilobyte22.kibibyte.api.CommandSender;
import de.kilobyte22.kibibyte.api.configuration.ConfigFile;
import de.kilobyte22.kibibyte.api.configuration.ConfigList;
import de.kilobyte22.kibibyte.api.configuration.ConfigMap;
import de.kilobyte22.kibibyte.api.configuration.ConfigNode;
import de.kilobyte22.kibibyte.api.core.SystemLogger;
import de.kilobyte22.kibibyte.api.event.BotSavingEvent;
import de.kilobyte22.kibibyte.api.event.EventBus;
import de.kilobyte22.kibibyte.api.event.RehashEvent;
import de.kilobyte22.kibibyte.api.event.ShutdownEvent;
import de.kilobyte22.kibibyte.api.hook.Hook;
import de.kilobyte22.kibibyte.api.permission.AccountHandler;
import de.kilobyte22.kibibyte.api.plugin.PluginSystem;
import de.kilobyte22.kibibyte.api.registry.AuthRegistry;
import org.pircbotx.Channel;
import org.pircbotx.User;
import org.pircbotx.exception.IrcException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.naming.OperationNotSupportedException;
import java.io.File;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;

public class Botmanager {

    private static Logger logger = LoggerFactory.getLogger("BOTMGR");
    public static final EventBus eventBus = new de.kilobyte22.kibibyte.event.EventBus();
    public static final ConfigFile config = new ConfigFile(new File("config.yml"));
    public static final ConfigFile ignoreFile = new ConfigFile(new File("ignore.yml"));
    private static final Map<String, Kibibyte> bots = new HashMap<String, Kibibyte>();
    public static final PluginSystem pluginSystem = new de.kilobyte22.kibibyte.plugin.PluginSystem();
    public static final SystemLogger syslog = new SystemLoggerImpl("core");
    private static int restartCode = 0;
    private static Map<Class<?extends Hook>, List<Hook>> hooks = new HashMap<>();

    static {
        try {
            restartCode = Integer.parseInt(System.getProperty("de.kilobyte22.restartExitCode", "0"));
        } catch (NumberFormatException ignored) {}
    }

    public static boolean canRestart() {
        return restartCode != 0;
    }

    public static Map<String, Kibibyte> getAllBots() {
        return bots;
    }

    public static void loadConfig() {
        logger.info("Loding global configuration...");
        config.load();
        if (config.isEmpty())
            generateDefaultConfig();
        ignoreFile.load();

        logger.info("Done.");

        ScheduledExecutorService scheduler = Executors.newScheduledThreadPool(1);

        final Runnable beeper = new Runnable() {
            public void run() {
                eventBus.post(new BotSavingEvent());
            }
        };
        final ScheduledFuture<?> beeperHandle = scheduler.scheduleAtFixedRate(beeper, 5, 5, TimeUnit.MINUTES);
    }

    private static void generateDefaultConfig() {
        ConfigMap exampleServer = new ConfigMap();
        config.getMap("servers")
                .putChained("example", exampleServer);
        config.putChained("prefixes", new ConfigList().addChained("#"));
        exampleServer
                .setString("displayName", "Example Server")
                .setString("host", "irc.example.net")
                .setInteger("port", 6667)
                .setString("nick", "Kibibyte")
                .makeMap("channels")
                .setString("nickserv", "");
        config.save();
    }

    public static void applyConfig() {
        ConfigMap servers = config.getMap("servers");
        for (String id : servers.keySet()) {
            ConfigMap server = servers.getMap(id);
            if (!server.getBoolean("enabled", true)) continue;
            ConfigMap imap = ignoreFile.getMap(id);
            imap.getList("nicks");
            imap.getList("accounts");
            Kibibyte bot = new Kibibyte(id);
            bot.setConfig(server);
            try {
                bot.setName(server.getString("nick", "Kibiybte"));
                bot.setVerbose(server.getBoolean("verbose", false));
                bot.connect(server.getString("host"), server.getInt("port", 6667));
                server.getMap("channels");
                bot.setAccountHandler(AuthRegistry.getAccountHandler(server.getString("accountHandler", "nickserv")));
                bot.setPermissionHandler(AuthRegistry.getPermissionHandler(server.getString("permissionHandler", "yaml")));
                bots.put(id, bot);
                String ns = server.getString("nickserv");
                if (ns.length() > 0)
                    bot.sendMessage("NickServ", "IDENTIFY " + ns);
            } catch (Exception e) {
                logger.error("", e);
            }
        }
        logger.info("Loading plugins...");
        for (ConfigNode node : config.getList("plugins")) {
            try {
                pluginSystem.loadPlugin(node.toString());
            } catch (Exception e) {
                logger.error("Failed to load plugin", e);
            }
        }
        try {
            Thread.sleep(500);
        } catch (InterruptedException e) {
            logger.error("", e);
        }
        for (Kibibyte kibi : bots.values()) {
            ConfigMap cfg = kibi.getConfig();
            if (cfg.containsKey("restart-info")) {
                ConfigMap restartInfo = cfg.getMap("restart-info");
                String who = restartInfo.getString("issuer");
                String msg = String.format("Restart successful... Took %s Seconds", (System.currentTimeMillis() - restartInfo.getLong("timestamp")) / 1000);
                kibi.sendNotice(who, msg);
                cfg.remove("restart-info");
                config.save();
            }
            ConfigMap channels = cfg.getMap("channels");
            for (String name : channels.keySet()) {
                ConfigMap channel = channels.getMap(name);
                if (channel.containsKey("pass"))
                    kibi.joinChannel(name, channel.getString("pass"));
                else
                    kibi.joinChannel(name);
            }

        }
        rehashCommon();
        config.save();
        logger.info("Done.");
    }

    private static void rehashCommon() {
        String logchan = config.getString("logchannel", "");
        if (logchan.contains("#")) {
            String[] parts = logchan.split("#");
            String net = parts[0];
            String chan = "#" + parts[1];
            Kibibyte bot = bots.get(net);
            if (bot == null) {
                logger.error("Unknown server for logchannel: %s", net);
            } else {
                Channel c = bot.getChannel(chan);
                if (c == null) {
                    bot.joinChannel(chan);
                }
                SyslogHandler.instance.setLogchannel(c);
            }
        }
    }

    public static boolean isIgnored(User user) {
        // if (((Kibibyte) user.getBot()).getServiceBots().contains(user.getNick().toLowerCase())) return true;
        if (ignoreFile.getMap(user.getBot().toString()).getList("nicks").containsObject(user.getNick().toLowerCase()))
            return true;
        Kibibyte k = (Kibibyte) user.getBot();
        AccountHandler h = (k).getAccountHandler();
        if (!h.isCaching(k)) return false;
        String a = k.getAccount(user);
        if (a == null) return false;
        return ignoreFile.getMap(k.getId()).getList("accounts").containsObject(a.toLowerCase());
    }

    public static void ignore(User user, boolean account) {
        Kibibyte b = (Kibibyte) user.getBot();
        ConfigMap i = ignoreFile.getMap(b.getId());
        if (account) {
            ConfigList l = i.getList("accounts");
            String name = b.getAccount(user);
            if (name == null)
                name = user.getNick();
            name = name.toLowerCase();

            if (l.containsObject(name))
                return;
            l.add(name);
        } else {
            ConfigList l = i.getList("nicks");
            String name = user.getNick().toLowerCase();
            if (l.containsObject(name))
                return;
            l.add(name);
        }
        ignoreFile.save();
    }

    public static void unignore(User user, boolean account) {
        Kibibyte b = (Kibibyte) user.getBot();
        ConfigMap i = ignoreFile.getMap(b.getId());
        if (account) {
            ConfigList l = i.getList("accounts");

            String name = b.getAccount(user);
            if (name == null)
                name = user.getNick();
            name = name.toLowerCase();

            if (!l.containsObject(name))
                return;
            l.removeObject(name);
        } else {
            ConfigList l = i.getList("nicks");
            String name = user.getNick().toLowerCase();
            if (!l.containsObject(name))
                return;
            l.removeObject(name);
        }
        ignoreFile.save();
    }

    public static void shutdown(String message, boolean restart) {
        syslog.info("Shutting down bot: %s");
        new ShutdownThread(message, restart).start();
    }

    private static class ShutdownThread extends Thread {


        private String message;
        private boolean restart;

        public ShutdownThread(String m, boolean r) {
            message = m;
            restart = r;
            setDaemon(false);
        }

        public void run() {
            // Send shutdown event
            eventBus.post(new ShutdownEvent(message));
            for (Kibibyte bot : bots.values()) {
                bot.quitServer(message);
            }
            try {
                Thread.sleep(2000); // Wait for everything to terminate, if done threaded for some reason
            } catch (InterruptedException e) {
                logger.warn("", e);
            }
            for (Kibibyte bot : bots.values()) {
                bot.shutdown(true);
            }

            System.exit(restart ? restartCode : 0);
        }
    }

    private static class BotShutdown extends Thread {
        private final String message;
        private List<Kibibyte> botlist = new LinkedList<>();
        public BotShutdown(Kibibyte bot, String message) {
            this.message = message;
            botlist.add(bot);
        }

        public BotShutdown(List<Kibibyte> bots, String message) {
            this.botlist = bots;
            this.message = message;
        }

        public void run() {
            for (Kibibyte bot : botlist) {
                bot.quitServer(message);
            }
            try {
                Thread.sleep(2000); // Wait for everything to terminate, if done threaded for some reason
            } catch (InterruptedException e) {
                logger.warn("", e);
            }
            for (Kibibyte bot : botlist) {
                bot.shutdown(true);
                bots.remove(bot.getId());
            }
        }
    }

    public static void rehash() {
        rehash(null);
    }

    public static void rehash(CommandSender sender) {
        sender = sender == null ? Console.console : sender;
        syslog.info("%s is rehashing config...");
        // copying old config
        ConfigMap old = config.shallowClone();

        // actual rehash
        config.load();

        ConfigMap nservers = config.getMap("servers");
        ConfigMap oservers = config.getMap("servers");

        // compare server list
        List<Kibibyte> botsToExit = new LinkedList<>();
        List<Kibibyte> botsToRehash = new LinkedList<>();
        for (String s : old.keySet()) {
            Kibibyte bot = bots.get(s);
            if (bot == null) continue;
            if (nservers.containsKey(s) && nservers.getMap(s).getBoolean("enabled")) {
                botsToRehash.add(bot);
            } else {
                botsToExit.add(bot);
            }
        }
        new BotShutdown(botsToExit, "Configuration Adjustment").start();
        for (Kibibyte bot : botsToRehash) {
            ConfigMap ocfg = old.getMap("servers").getMap(bot.getId());
            ConfigMap ncfg = config.getMap("servers").getMap(bot.getId());
            bot.setConfig(ncfg);
            bot.setVerbose(ncfg.getBoolean("verbose", false));
            boolean needsRestart = !(
                    ocfg.getString("host").equals(ncfg.getString("host")) &&
                    ocfg.getInt("port").equals(ncfg.getInt("port")));
            if (needsRestart) {
                bot.quitServer("Reconnect: Configuration change");
                bot.setName(ncfg.getString("nick"));
                try {
                    Thread.sleep(500);
                } catch (InterruptedException e) {}
                try {
                    bot.connect(ncfg.getString("host"), ncfg.getInt("port"));
                } catch (Exception e) {
                    logger.error("Error when reconnecting " + bot.getId(), e);
                }
                continue;
            }
            if (!ocfg.getString("nick").equals(ncfg.getString("nick"))) {
                bot.changeNick(ncfg.getString("nick"));
            }

            // comparing channels
            ConfigMap ochan = ocfg.getMap("channels"), nchan = ncfg.getMap("channels");
            for (String channel : ochan.keySet()) {
                if (!nchan.containsKey(channel)) {
                    try {
                        bot.partChannel(bot.getChannel(channel), "Configuration change");
                    } finally {}
                }
            }

            for (String channel : nchan.keySet()) {
                if (!ochan.containsKey(channel)) {
                    ConfigMap ccfg = nchan.getMap(channel);
                    if (ccfg.containsKey("password"))
                        bot.joinChannel(channel, ccfg.getString("password"));
                    else
                        bot.joinChannel(channel);
                }
            }
            rehashCommon();
            try {
                bot.setAccountHandler(AuthRegistry.getAccountHandler(ncfg.getString("accountHandler", "nickserv")));
            } catch (OperationNotSupportedException e) {
                syslog.warn("Invalid Account Handler for network %s: %s", bot.getDisplayName(), ncfg.getString("accountHandler", "nickserv"));
            }
            bot.setPermissionHandler(AuthRegistry.getPermissionHandler(ncfg.getString("permissionHandler", "yaml")));
        }
        Botmanager.eventBus.post(new RehashEvent(sender));
    }

    public static List<Hook> getHook(Class<?extends Hook> clazz) {
        return hooks.get(clazz);
    }

    public static void doHook(Class<?extends Hook> clazz, Object event) {
        List<Hook> hooks = getHook(clazz);
        if (hooks == null) return;
        try {
            Method m = clazz.getDeclaredMethod("process", event.getClass());
            m.setAccessible(true);
            for (Hook h : hooks) {
                m.invoke(h, event);
            }
        } catch (NoSuchMethodException | IllegalAccessException | InvocationTargetException e) {
            throw new RuntimeException(e);
        }

    }

    public static void registerHook(Hook hook) {
        Class<?extends Hook> c = (Class<? extends Hook>) hook.getClass().getInterfaces()[0];
        if (!hooks.containsKey(c))
            hooks.put(c, new LinkedList<Hook>());
        List<Hook> hooks = getHook(c);
        hooks.add(hook);
    }

}
