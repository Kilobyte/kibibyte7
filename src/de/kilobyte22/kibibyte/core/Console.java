package de.kilobyte22.kibibyte.core;

import de.kilobyte22.kibibyte.CommandManagerImpl;
import de.kilobyte22.kibibyte.api.CommandSender;
import de.kilobyte22.kibibyte.api.command.CommandInfo;
import de.kilobyte22.kibibyte.api.command.CommandManager;
import de.kilobyte22.kibibyte.api.exception.CommandNotFoundException;
import de.kilobyte22.kibibyte.api.registry.CommandRegistry;
import org.pircbotx.Channel;
import org.pircbotx.User;

import java.util.Scanner;

public class Console extends Thread {

    public static CommandSender console = new CommandSender() {
        @Override
        public void sendMessage(String message, String... params) {
            for (String m : String.format(message, ((Object[]) params)).split("\n"))
                System.out.println("[CMD] " + m);
        }

        @Override
        public String getAccount() {
            return "CLI";
        }

        @Override
        public boolean hasPermission(String permission) {
            return true; // cosole overrides all permissions
        }

        @Override
        public User getUser() {
            return null;
        }

        @Override
        public String getName() {
            return "Console";
        }

        @Override
        public void reportError(String message, String... params) {
            System.err.println("[CMD] " + String.format(message, ((Object[]) params)));
        }

        @Override
        public String getLocation() {
            return "CLI";
        }

        @Override
        public String networkID() {
            return "CLI";
        }

        @Override
        public boolean canSee(Channel channel) {
            return true;
        }

        @Override
        public String getFullAccount() {
            return "Console@CLI";
        }
    };

    @Override
    public void run() {
        Scanner scanner = new Scanner(System.in);
        CommandManagerImpl mgr = new CommandManagerImpl();
        while (true) {
            String line = scanner.nextLine();
            try {
                mgr.doCommand(line, null, null, console, true);
            } catch (Throwable e) {
                e.printStackTrace();
            }
        }

    }

}
