package de.kilobyte22.kibibyte.core;

import com.google.common.eventbus.Subscribe;
import de.kilobyte22.kibibyte.api.bufferplayback.*;
import de.kilobyte22.kibibyte.api.configuration.ConfigMap;
import de.kilobyte22.kibibyte.api.configuration.ConfigNode;
import de.kilobyte22.kibibyte.api.event.CapHandlerRegistrationHook;
import de.kilobyte22.kibibyte.api.event.ConnectionDoneEvent;
import de.kilobyte22.kibibyte.api.hook.Hook;
import de.kilobyte22.kibibyte.api.permission.AccountHandler;
import de.kilobyte22.kibibyte.api.permission.PermissionHandler;
import de.kilobyte22.kibibyte.api.registry.PlaybackRegistry;
import de.kilobyte22.kibibyte.event.BotListener;
import de.kilobyte22.kibibyte.permission.PermissionHelper;
import org.pircbotx.Channel;
import org.pircbotx.CustomInputThread;
import org.pircbotx.InputThread;
import org.pircbotx.User;
import org.pircbotx.cap.CapHandler;
import org.pircbotx.cap.EnableCapHandler;
import org.pircbotx.exception.IrcException;
import org.pircbotx.hooks.events.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.naming.OperationNotSupportedException;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.net.Socket;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;


public class Kibibyte extends de.kilobyte22.kibibyte.api.core.Kibibyte {

    public final String ID;
    private final Logger logger;
    private ConfigMap config;
    private List<String> requestedCap = new LinkedList<String>();
    private AccountHandler accountHandler;
    private PermissionHelper permissionHelper = new PermissionHelper(this);
    private PermissionHandler permissionHandler;
    private String displayName;
    private LinkedList<String> serviceBots;
    private boolean followInvites;

    Kibibyte(String ID) {
        this.getListenerManager().addListener(new BotListener());
        this.ID = ID;
        logger = LoggerFactory.getLogger("kibi-" + ID);
        setCapEnabled(true);
        Botmanager.eventBus.register(this);
    }

    @Override
    public void connect(String host, int port) throws IrcException, IOException {
        Botmanager.eventBus.post(new CapHandlerRegistrationHook(this));
        logger.info("Connecting to {}:{}...", host.contains(":") ? "[" + host + "]" : host, port);
        super.connect(host, port);
        Botmanager.eventBus.post(new ConnectionDoneEvent(this));
    }

    public ConfigMap getConfig() {
        return Botmanager.config.getMap("servers").getMap(ID);
    }

    @Override
    public String stripPrefix(String message, Channel channel) {
        String[] prefixes = getPrefixes();
        for (String p : prefixes) {
            if (!message.startsWith(p)) continue;
            return message.substring(p.length());
        }
        return null;
    }

    @Override
    public String[] getPrefixes() {
        ArrayList<String> tmp = new ArrayList();
        ConfigMap map = getConfig();
        if (map.containsKey("prefixes") && !map.getList("prefixes").isEmpty()) {
            for (ConfigNode n : map.getList("prefixes"))
                tmp.add(n.toString());
        } else {
            for (ConfigNode n : Botmanager.config.getList("prefixes"))
                tmp.add(n.toString());
        }
        return tmp.toArray(new String[0]);
    }

    @Override
    public void doDefaultMessage(User user, String message) {
        sendNotice(user, message);
    }

    @Override
    public String getId() {
        return ID;
    }

    @Override
    public void requestCap(String... caps) {
        for (String cap : caps) {
            if (!requestedCap.contains(cap)) {
                requestedCap.add(cap);
                getCapHandlers().add(new EnableCapHandler(cap, true));
            }
        }
    }

    @Override
    public Boolean hasCap(String name) {
        for (CapHandler handler : getCapHandlers()) {
            if (handler instanceof EnableCapHandler) {
                EnableCapHandler eh = (EnableCapHandler) handler;
                if (eh.getCap().equals(name))
                    return eh.isDone();
            }
        }
        return null;
    }

    public void setConfig(ConfigMap config) {
        this.config = config;
        displayName = config.getString("displayName");
        this.serviceBots = new LinkedList<>();
        for (ConfigNode node : config.getList("serviceBots")) {
            serviceBots.add(node.toString());
        }
        followInvites = config.getBoolean("followInvited", true);
    }

    public String getAccount(User user) {
        return accountHandler.getAccount(user);
    }

    @Override
    public void setAccountHandler(AccountHandler accountHandler) throws OperationNotSupportedException {
        this.accountHandler = accountHandler;
        accountHandler.setupNetwork(this);
    }

    @Override
    public boolean hasPermission(User user, Channel channel, String permission) {
        //TODO: implement
        return permissionHelper.hasPermission(getAccount(user), channel, permission, false);
    }

    @Override
    public PermissionHandler getPermissionHandler() {
        return permissionHandler;
    }

    @Override
    public void setPermissionHandler(PermissionHandler handler) {
        if (permissionHandler != null)
            permissionHandler.removeHandledServer(this);
        permissionHandler = handler;
        permissionHandler.addHandledServer(this);
    }

    @Override
    public String getDisplayName() {
        return displayName;
    }

    @Override
    public List<String> getServiceBots() {
        return serviceBots;
    }

    @Subscribe
    public void onJoin(JoinEvent event) {
        final Channel c = event.getChannel();
        if (event.getUser() == getUserBot()) {
            ConfigMap ccfg = config.getMap("channels");
            if (!ccfg.containsKey(c.getName())) {
                ccfg.put(c.getName(), ccfg.getMap(":default"));
            }
            ConfigMap channel = ccfg.getMap(c.getName());
            PlaybackRegistry.setChannelHandler(c, channel.getString("playbackHandler", "ram"));
        }
        Message m = new JoinMessage(event);
        m.setIgnored(Botmanager.isIgnored(event.getUser()));
        PlaybackRegistry.getHandler(c).appendMessage(c, m);
    }

    @Subscribe
    public void onMessage(MessageEvent event) {
        final Channel c = event.getChannel();
        Message m = new RegularMessage(event);
        m.setIgnored(Botmanager.isIgnored(event.getUser()));
        PlaybackRegistry.getHandler(c).appendMessage(c, m);
    }

    @Subscribe
    public void onAction(ActionEvent event) {
        final Channel c = event.getChannel();
        Message m = new ActionMessage(event);
        m.setIgnored(Botmanager.isIgnored(event.getUser()));
        PlaybackRegistry.getHandler(c).appendMessage(c, m);
    }

    @Subscribe
    public void onKick(KickEvent event) {
        final Channel c = event.getChannel();
        PlaybackRegistry.getHandler(c).appendMessage(c, new KickMessage(event));
        if (event.getRecipient() == event.getBot().getUserBot()) {
            getConfig().getMap("channels").remove(event.getChannel().getName());
            Botmanager.config.save();
        }
    }

    @Override
    public String toString() {
        return getId();
    }

    public AccountHandler getAccountHandler() {
        return accountHandler;
    }

    @Override
    protected InputThread createInputThread(Socket socket, BufferedReader reader) {
        InputThread input = new CustomInputThread(this, socket, reader);
        input.setName("bot" + botCount + "-input");
        return input;
    }

    @Subscribe
    public void onInvite(InviteEvent event) {
        if (followInvites)
            joinChannel(event.getChannel());
    }
}
