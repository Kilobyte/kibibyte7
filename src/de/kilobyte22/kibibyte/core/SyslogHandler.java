package de.kilobyte22.kibibyte.core;

import de.kilobyte22.kibibyte.api.IrcColor;
import org.pircbotx.Channel;

public class SyslogHandler {
    public static final SyslogHandler instance = new SyslogHandler();
    private Channel logchan;
    public SyslogHandler() {}

    public void info(String module, String message, String... args) {
        log(module, "info", null, message, args);
    }

    public void warn(String module, String message, String... args) {
        log(module, "warn", IrcColor.ORANGE, message, args);
    }

    public void error(String module, String message, String... args) {
        log(module, "error", IrcColor.RED, message, args);
    }

    public void severe(String module, String message, String... args) {
        log(module, "severe", IrcColor.PURPLE, message, args);
    }

    private void log(String module, String type, IrcColor color, String message, String... args) {
        String c = color != null ? IrcColor.COLOR.toString() + color.toString() : "";
        try {
            logchan.getBot().sendMessage(logchan, String.format("%s%s: %s: %s", c, module, type, String.format(message, ((Object[]) args))));
        } catch (Exception ex) {}
    }

    public void setLogchannel(Channel c) {
        logchan = c;
    }
}
