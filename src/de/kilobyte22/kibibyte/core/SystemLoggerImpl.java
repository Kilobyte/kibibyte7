package de.kilobyte22.kibibyte.core;

import de.kilobyte22.kibibyte.api.core.SystemLogger;

public class SystemLoggerImpl implements SystemLogger {

    private final String name;

    public SystemLoggerImpl(String name) {
        this.name = name;
    }

    @Override
    public void info(String message, String... args) {
        SyslogHandler.instance.info(name, message, args);
    }

    @Override
    public void warn(String message, String... args) {
        SyslogHandler.instance.warn(name, message, args);
    }

    @Override
    public void error(String message, String... args) {
        SyslogHandler.instance.error(name, message, args);
    }

    @Override
    public void severe(String message, String... args) {
        SyslogHandler.instance.severe(name, message, args);
    }
}
