package de.kilobyte22.kibibyte.event;

import de.kilobyte22.kibibyte.core.Botmanager;
import org.pircbotx.hooks.Event;
import org.pircbotx.hooks.Listener;

public class BotListener implements Listener {
    @Override
    public void onEvent(Event event) throws Exception {
        Botmanager.eventBus.post(event);
    }
}
