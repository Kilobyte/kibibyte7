package de.kilobyte22.kibibyte.event;

import com.google.common.eventbus.Subscribe;
import de.kilobyte22.kibibyte.api.NeedsPermission;
import de.kilobyte22.kibibyte.api.event.EventFilter;
import de.kilobyte22.kibibyte.api.event.ExtendedSubscribe;
import de.kilobyte22.kibibyte.api.registry.EventRegistry;
import de.kilobyte22.kibibyte.core.Kibibyte;
import org.pircbotx.Channel;
import org.pircbotx.PircBotX;
import org.pircbotx.User;
import org.pircbotx.hooks.Event;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.*;
import java.util.concurrent.locks.ReentrantLock;

public class EventBus implements de.kilobyte22.kibibyte.api.event.EventBus {

    private Map<Class, List<HandlerInfo>> handlers = new HashMap<Class, List<HandlerInfo>>();
    private volatile boolean isDoingEventQueue = false;
    private Queue<Object> eventQueue = new LinkedList<Object>();
    private ReentrantLock eventQueueLock = new ReentrantLock();
    private static final Logger logger = LoggerFactory.getLogger("EVENTBUS");

    @Override
    public void post(Object event) {
        eventQueueLock.lock();
        eventQueue.offer(event);
        eventQueueLock.unlock();
        doEventQueue();
    }

    @Override
    public void register(Object handler) {
        Class clazz;
        boolean static_ = false;
        if (handler instanceof Class) {
            clazz = (Class) handler;
            static_ = true;
        } else {
            clazz = handler.getClass();
        }
        for (Method m : clazz.getMethods()) {
            boolean extended = false;
            if (Modifier.isStatic(m.getModifiers()) != static_) continue;
            if (m.isAnnotationPresent(Subscribe.class) || (extended = m.isAnnotationPresent(ExtendedSubscribe.class))) {
                if (m.getParameterTypes().length != 1)
                    throw new RuntimeException("Invalid Handler: Method " + m.getName() + " is annotated with @" + (extended ? "Extended" : "") + "Subscribe, but has != 1 parameter");
                HandlerInfo info = new HandlerInfo();
                info.handler = handler;
                info.callback = m;
                if (extended) {
                    ExtendedSubscribe e = m.getAnnotation(ExtendedSubscribe.class);
                    info.filters = new FilterInfo[e.filters().length];
                    int i = 0;
                    for (String s : e.filters()) {
                        FilterInfo finfo = new FilterInfo();
                        String filter = s.split(":")[0];
                        finfo.filter = EventRegistry.getFilter(filter);
                        if (finfo.filter == null)
                            throw new RuntimeException("Unknown Filter: " + filter);
                        if (s.equals(filter))
                            finfo.arg = "";
                        else
                            finfo.arg = s.substring(filter.length() + 1);
                        info.filters[i++] = finfo;
                    }
                    info.filterLinking = e.filterLinking();
                }
                if (m.isAnnotationPresent(NeedsPermission.class)) {
                    info.permission = m.getAnnotation(NeedsPermission.class).value();
                }
                m.setAccessible(true);
                addHandler(m.getParameterTypes()[0], info);
            }
        }
    }

    private void addHandler(Class clazz, HandlerInfo info) {
        if (!handlers.containsKey(clazz))
            handlers.put(clazz, new LinkedList<HandlerInfo>());
        handlers.get(clazz).add(info);
    }

    @Override
    public void unregister(Object handler) {
    }

    private void doEventQueue() {
        if (isDoingEventQueue) return;
        isDoingEventQueue = true;
        try {
            eventQueueLock.lock();
            while (!eventQueue.isEmpty()) {
                Object event = eventQueue.poll();
                eventQueueLock.unlock();
                Class clazz = event.getClass();
                Kibibyte bot = null;
                User user = null;
                Channel channel = null;
                if (event instanceof  Event) {
                    bot = ((Kibibyte) ((Event) event).getBot());
                    try {
                        user = (User) clazz.getMethod("getUser").invoke(event);
                        channel = (Channel) clazz.getMethod("getChannel").invoke(event);
                    } catch (Exception e) {}
                }
                while (true) {
                    List<HandlerInfo> infoList = handlers.get(clazz);
                    if (infoList != null) {
                        for (HandlerInfo info : infoList) {
                            boolean canPass = true;
                            for (FilterInfo finfo : info.filters) {
                                if (finfo.filter.canPass(event, finfo.arg)) {
                                    if (info.filterLinking == ExtendedSubscribe.FilterLinking.OR) {
                                        canPass = true;
                                        break;
                                    }
                                } else {
                                    if (info.filterLinking == ExtendedSubscribe.FilterLinking.AND) {
                                        canPass = false;
                                        break;
                                    }
                                }
                            }
                            if (!canPass) continue;
                            if (!passesCustomFilter(info, event, bot, channel, user)) continue;
                            if (info.permission != null && user != null) {
                                if (!((Kibibyte) ((Event) event).getBot()).hasPermission(user, channel, info.permission)) continue;
                            }
                            try {
                                info.callback.invoke(info.handler, event);
                            } catch (IllegalAccessException e) {
                                logger.error("Internal error", e);
                            } catch (InvocationTargetException e) {
                                logger.error("Unhandled exception in event handler", e.getCause());
                            }
                        }
                    }
                    if (clazz == Object.class) break;
                    clazz = clazz.getSuperclass();
                }
                eventQueueLock.lock();
            }
            eventQueueLock.unlock();
        } catch (RuntimeException e) {
            isDoingEventQueue = false;
            if (eventQueueLock.isHeldByCurrentThread())
                eventQueueLock.unlock();
            throw e;
        }
        isDoingEventQueue = false;
    }

    protected boolean passesCustomFilter(HandlerInfo info, Object event, Kibibyte bot, Channel channel, User user) {
        return true;
    }

    protected class HandlerInfo {
        public FilterInfo[] filters = new FilterInfo[0];
        public Object handler;
        public Method callback;
        public String permission = null;
        public ExtendedSubscribe.FilterLinking filterLinking;
    }

    protected class FilterInfo {
        public EventFilter filter;
        public String arg;
    }
}
