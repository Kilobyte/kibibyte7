package de.kilobyte22.kibibyte.event.filter;

import de.kilobyte22.kibibyte.api.event.EventFilter;
import org.pircbotx.hooks.events.MessageEvent;

public class MessageFilter implements EventFilter {
    @Override
    public boolean canPass(Object event, String param) {
        if (event instanceof MessageEvent) {
            MessageEvent e = (MessageEvent) event;
            return e.getMessage().matches(param);
        }
        return false;
    }

    @Override
    public String getName() {
        return "message";
    }
}
