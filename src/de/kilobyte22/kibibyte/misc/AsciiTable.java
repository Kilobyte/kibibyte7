package de.kilobyte22.kibibyte.misc;

import com.sun.xml.internal.ws.util.StringUtils;
import de.kilobyte22.kibibyte.Util;

import java.util.LinkedList;
import java.util.List;

/**
 * This calls creates a table and allows to render it as ASCII
 */
public class AsciiTable {

    final String[] headers;
    boolean borders[] = new boolean[]{true, true, true};
    private List<String[]> lines = new LinkedList<>();
    private int[] colwidth;
    private String renderCache = null;

    public AsciiTable(String... headers) {
        this.headers = headers;
    }

    @Deprecated
    public AsciiTable addColoumn(String... fields) {
        return addRow(fields);
    }

    public AsciiTable addRow(String... fields) {
        String[] cp = new String[headers.length];
        final int len = cp.length;
        for (int i = 0; i < fields.length; i++) {
            if (i >= len) {
                cp[i] = "";
            } else {
                cp[i] = fields[i];
            }
        }
        lines.add(cp);
        renderCache = null;
        return this;
    }

    public void setBorders(Align align, boolean state) {
        borders[align.ordinal()] = state;
        renderCache = null;
    }

    public synchronized String render() {
        if (renderCache != null) return renderCache;
        final int len = headers.length;
        String ret = "";
        colwidth = new int[headers.length];
        for (int i = 0; i < headers.length; i++) {
            colwidth[i] = headers[i].length();
        }

        for (int i = 0; i < len; i++) {
            int w = colwidth[i];
            for (String[] arr : lines) {
                String el = arr[i];
                if (el.length() > w)
                    w = el.length();
            }
            colwidth[i] = w;
        }

        int totalwidth = 0;
        for (int i : colwidth) {
            totalwidth += i;
        }

        // Now that we have the layout we can actually start rendering

        ret += renderLine(headers);
        ret += "\n" + Util.repeatString("-", totalwidth);

        for (String[] line : lines) {
            ret += "\n" + renderLine(line);
        }
        renderCache = ret;
        return ret;
    }

    private String renderLine(String[] line) {
        String ret = "";
        for (int i = 0; i < line.length; i++) {
            String el = line[i];
            ret += el;
            if (i + 1 == line.length) break;
            ret += Util.repeatString(" ", colwidth[i] - el.length());
            ret += "  ";
        }
        return ret;
    }

    private enum Align {
        FRAME
    }
}
