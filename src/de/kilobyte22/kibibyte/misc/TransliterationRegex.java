package de.kilobyte22.kibibyte.misc;


public class TransliterationRegex {

    private final String search;
    private final String replace;
    private final int replen;

    public TransliterationRegex(String search, String replace) throws Exception {
        this.search = expandRegex(search);
        this.replace = expandRegex(replace);
        replen = this.replace.length();
    }

    public String applyTo(String data) {
        String out = "";
        for (int i = 0; i < data.length(); i++) {
            char c = data.charAt(i);
            int idx = search.indexOf(c);
            if (idx == -1)
                out += c;
            else
                out += replace.charAt(idx % replen);
        }
        return out;
    }

    public static String expandRegex(String in) throws Exception {
        String out = "";
        char last = 0;
        boolean range = false;
        boolean escaped = false;
        for (int i = 0; i < in.length(); i++) {
            char c = in.charAt(i);
            if (c != '-' && !range && last != 0) {
                out += last;
                last = 0;
            }
            switch (c) {
                case '-':
                    if (escaped) last = '-'; else {
                        if (range || last == 0) throw new Exception("At position %s: unexpected token -");
                        range = true;
                    }
                    break;
                case '\\':
                    if (escaped)
                        last = '\\';
                    else
                        escaped = true;
                    break;
                default:
                    if (range) {
                        int step = c > last ? 1 : -1;
                        for (char a = last; a != c; a += step) {
                            out += a;
                        }
                        out += c;
                        last = 0;
                        range = false;
                    } else {
                        last = c;
                    }
            }
        }
        if (last != 0) out += last;
        if (range || escaped) throw new Exception("Unexpected End of Pattern");
        return out;
    }

}
