package de.kilobyte22.kibibyte.permission;

import com.google.common.eventbus.Subscribe;
import de.kilobyte22.kibibyte.api.configuration.ConfigMap;
import de.kilobyte22.kibibyte.api.core.Kibibyte;
import de.kilobyte22.kibibyte.api.event.AccountChangedEvent;
import de.kilobyte22.kibibyte.api.event.CapHandlerRegistrationHook;
import de.kilobyte22.kibibyte.api.permission.AccountHandler;
import de.kilobyte22.kibibyte.core.Botmanager;
import org.pircbotx.PircBotX;
import org.pircbotx.User;
import org.pircbotx.cap.EnableCapHandler;
import org.pircbotx.hooks.events.JoinEvent;
import org.pircbotx.hooks.events.ServerResponseEvent;
import org.pircbotx.hooks.events.UnknownEvent;

import javax.naming.OperationNotSupportedException;
import java.util.HashMap;
import java.util.Map;
import java.util.WeakHashMap;

public class NickservAccountHandler implements AccountHandler {

    OldschoolOne backendOldschool = new OldschoolOne();
    Ircv3One backendIrcv3 = new Ircv3One();
    Map<Kibibyte, AccountHandler> backendLookup = new WeakHashMap<>();

    public NickservAccountHandler() {
        Botmanager.eventBus.register(this);
        Botmanager.eventBus.register(backendIrcv3);
        Botmanager.eventBus.register(backendOldschool);
    }

    @Override
    public String getName() {
        return "nickserv";
    }

    @Override
    public String getAccount(User user) {
        return backendLookup.get(user.getBot()).getAccount(user);
    }

    @Override
    public boolean isCaching(Kibibyte bot) {
        return backendLookup.get(bot).isCaching(bot);
    }

    @Override
    public void setConfig(Kibibyte bot, ConfigMap config) {
        backendLookup.get(bot).setConfig(bot, config);
    }

    @Override
    public void setupNetwork(Kibibyte bot) throws OperationNotSupportedException {
        if (bot.hasCap("account-notify")) {
            backendLookup.put(bot, backendIrcv3);
            backendIrcv3.setupNetwork(bot);
        } else {
            backendLookup.put(bot, backendOldschool);
            backendOldschool.setupNetwork(bot);
        }

    }

    @Subscribe
    public void onCapRegister(CapHandlerRegistrationHook hook) {
        hook.bot.requestCap("account-notify", "extended-join");
    }

    private class OldschoolOne implements AccountHandler {

        private Map<String, Thread> threads = new HashMap<>();
        private ConfigMap config;
        private Map<String, String> results = new HashMap<>();

        @Override
        public String getName() {
            return null;
        }

        @Override
        public String getAccount(User user) {
            String name = user.getBot().toString() + "/" + user.getNick();
            threads.put(name, Thread.currentThread());
            user.getBot().sendMessage(config.getString("nsbot"), "ACC " + user.getNick() + " *");
            try {
                Thread.sleep(config.getInt("nstimeout"));
            } catch (InterruptedException e) {}
            threads.remove(name);
            return results.get(name);
        }

        @Override
        public boolean isCaching(Kibibyte bot) {
            return false;
        }

        @Override
        public void setupNetwork(Kibibyte bot) throws OperationNotSupportedException {
            throw new OperationNotSupportedException("Nickserv requires IRCv3 atm");
        }

        @Override
        public void setConfig(Kibibyte bot, ConfigMap config) {
            this.config = config;
            config.getInt("nstimeout", 1000);
            config.getString("nsbot", "NickServ");
        }
    }


    private class Ircv3One implements AccountHandler {

        Map<Kibibyte, Map<User, String>> accounts = new WeakHashMap<Kibibyte, Map<User, String>>();
        private ConfigMap config;

        @Override
        public String getName() {
            return null;
        }

        @Override
        public String getAccount(User user) {
            return accounts.get(user.getBot()).get(user);
        }

        @Override
        public boolean isCaching(Kibibyte bot) {
            return true;
        }

        @Override
        public void setupNetwork(Kibibyte bot) throws OperationNotSupportedException {
            accounts.put(bot, new WeakHashMap<User, String>());
        }

        @Override
        public void setConfig(Kibibyte bot, ConfigMap config) {
            this.config = config;
        }

        //TODO: make @SubscribeRaw("ACCOUNT") once implemented
        @Subscribe
        public void onRaw(UnknownEvent event) {
            String[] parts = event.getLine().split(" ");
            if (parts.length == 3 && parts[1].equalsIgnoreCase("ACCOUNT")) {
                String nick = parts[0].split("!")[0];
                if (nick.startsWith(":")) nick = nick.substring(1);
                User user = event.getBot().getUser(nick);
                PircBotX bot = event.getBot();
                if (parts[2].equals("*"))
                    accounts.get(bot).remove(user);
                else
                    accounts.get(bot).put(user, parts[2]);
                Botmanager.eventBus.post(new AccountChangedEvent(user, parts[2]));
            }
        }

        @Subscribe
        public void onExtendedWho(ServerResponseEvent event) {
            if (event.getCode() != 354)
                return;
            if (!accounts.containsKey(event.getBot())) return;
            String parts[] = event.getResponse().split(" ");
            if (parts.length == 3) {
                User u =  event.getBot().getUser(parts[1]);
                if (u == null) return;
                if (parts[2].equals("0"))
                    accounts.get(event.getBot()).remove(u);
                else
                    accounts.get(event.getBot()).put(u, parts[2]);
            }
        }

        @Subscribe
        public void onJoin(JoinEvent event) {
            if (event.getUser() == event.getBot().getUserBot()) {
                event.getBot().sendRawLine("WHO " + event.getChannel().getName() + " %na");
            } else {
                event.getBot().sendRawLine("WHO " + event.getUser().getNick() + " %na");
            }
        }
    }
}
