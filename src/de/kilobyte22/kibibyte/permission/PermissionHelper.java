package de.kilobyte22.kibibyte.permission;

import de.kilobyte22.kibibyte.api.permission.Group;
import de.kilobyte22.kibibyte.core.Kibibyte;
import org.pircbotx.Channel;

public class PermissionHelper {
    private final Kibibyte bot;

    public PermissionHelper(Kibibyte bot) {

        this.bot = bot;
    }

    private Boolean userHasPermission(String user, String permission) {
        if (user == null) return null;
        String node = permission;
        do {
            String realnode = node + ((permission.equalsIgnoreCase(node) || node.equals("*")) ? "" : ".*");
            Boolean p = bot.getPermissionHandler().hasPermission(bot, user, realnode);
            if (p != null) return p;
            String[] nodehacked = node.split("\\.");
            try {
                node = node.substring(0, node.length() - nodehacked[nodehacked.length - 1].length() - 1);
            } catch (Exception ex) {
                node = (node.equals("*") ? "" : "*");
            }
        } while (!( node.equals("")));
        return null;
    }

    private Boolean groupHasPermission(Group group, String permission) {
        if (group == null) return null;
        String node = permission;
        do {
            String realnode = node + ((permission.equalsIgnoreCase(node) || node.equals("*")) ? "" : ".*");
            Boolean p = group.hasPermission(realnode);
            if (p != null) return p;
            String[] nodehacked = node.split("\\.");
            try {
                node = node.substring(0, node.length() - nodehacked[nodehacked.length - 1].length() - 1);
            } catch (Exception ex) {
                node = (node.equals("*") ? "" : "*");
            }
        } while (!( node.equals("")));
        return null;
    }

    public boolean hasPermission(String account, Channel channel, String permission, boolean strict) {
        Boolean tmp = userHasPermission(account, permission);
        if (tmp != null) return tmp;
        for (Group group : bot.getPermissionHandler().getChannelGroups(channel, account)) {
            tmp = groupHasPermission(group, permission);
            if (tmp != null) return tmp;
        }
        for (Group group : bot.getPermissionHandler().getGroups(bot, account)) {
            tmp = groupHasPermission(group, permission);
            if (tmp != null) return tmp;
        }
        if (!strict) return hasPermission(":default", channel, permission, true);
        return false;
    }

}
