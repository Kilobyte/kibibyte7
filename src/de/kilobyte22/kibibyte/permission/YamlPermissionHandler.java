package de.kilobyte22.kibibyte.permission;

import de.kilobyte22.kibibyte.api.configuration.ConfigFile;
import de.kilobyte22.kibibyte.api.configuration.ConfigList;
import de.kilobyte22.kibibyte.api.configuration.ConfigMap;
import de.kilobyte22.kibibyte.api.configuration.ConfigNode;
import de.kilobyte22.kibibyte.api.core.Kibibyte;
import de.kilobyte22.kibibyte.api.permission.Group;
import de.kilobyte22.kibibyte.api.permission.PermissionHandler;
import org.pircbotx.Channel;

import java.io.File;
import java.util.*;

public class YamlPermissionHandler implements PermissionHandler {

    private ConfigFile permFile = new ConfigFile(new File("permissions.yml")); {permFile.load(); permFile.save();}
    private Map<String, Group> groups = new HashMap<>();
    private Map<Channel, Map<String, Group>> channelGroups = new WeakHashMap<>();

    @Override
    public String getName() {
        return "yaml";
    }

    @Override
    public Boolean hasPermission(Kibibyte network, String account, String permission) {
        ConfigMap users = permFile.getMap("servers").getMap(network.getId()).getMap("users");
        if (!users.containsKey(account)) return null;
        Boolean ret = users.getMap(account).getMap("permissions").getBoolean(permission);
        permFile.save();
        return ret;
    }

    @Override
    public void setPermission(Kibibyte network, String account, String permission, Boolean value) {
        ConfigMap perms = permFile.getMap("servers").getMap(network.getId()).getMap("users").getMap(account).getMap("permissions");
        if (value == null)
            perms.remove(permission);
        else
            perms.put(permission, value);
        permFile.save();
    }

    @Override
    public boolean isChannelPermission(String permission) {
        boolean ret = permFile.getList("channelperms").containsObject(permission);
        permFile.save();
        return ret;
    }

    @Override
    public List<String> getChannelPermissions() {
        List<String> ret = new LinkedList<>();
        for (ConfigNode node : permFile.getList("channelperms")) {
            ret.add(node.toString());
        }
        permFile.save();
        return ret;
    }

    @Override
    public void setChannelPermission(String permission, boolean value) {
        ConfigList channelPerms = permFile.getList("channelperms");
        if (channelPerms.containsObject(permission) == value)
            return;
        else if (value)
            channelPerms.add(permission);
        else
            channelPerms.removeObject(permission);
    }

    @Override
    public void removeHandledServer(Kibibyte bot) {
    }

    @Override
    public void addHandledServer(Kibibyte bot) {
    }

    @Override
    public List<Group> getGroups(Kibibyte network, String account) {
        ConfigMap users = permFile.getMap("servers").getMap(network.getId()).getMap("users");
        List<Group> ret = new LinkedList<>();
        if (!users.containsKey(account)) return ret;
        for (ConfigNode node : users.getMap(account).getList("groups")) {
            ret.add(getGroup(node.toString()));
        }
        return ret;
    }

    @Override
    public List<Group> getGroups() {
        List<Group> ret = new LinkedList<>();
        for (String name : groups.keySet()) {
            ret.add(groups.get(name));
        }
        return ret;
    }

    @Override
    public Group getGroup(String name) {
        return groups.get(name);
    }

    @Override
    public List<Group> getChannelGroups(Channel channel) {
        List<Group> ret = new LinkedList<>();
        if (!channelGroups.containsKey(channel))
            loadChannelGroups(channel);
        Map<String, Group> groups = channelGroups.get(channel);
        for (String name : groups.keySet()) {

        }
        return ret;
    }

    private void loadChannelGroups(Channel channel) {
        Map<String, Group> ret = new HashMap<>();
        ConfigMap groups = permFile.getMap("servers").getMap(((Kibibyte) channel.getBot()).getId()).getMap("channels").getMap("groups");
        for (String name : groups.keySet()) {
            ret.put(name, new GroupImpl(name));
        }

        channelGroups.put(channel, ret);
    }

    @Override
    public List<Group> getChannelGroups(Channel channel, String account) {
        return new LinkedList<>();
    }

    @Override
    public Group makeGroup(String name) {
        GroupImpl group = new GroupImpl(name);
        group.init();
        groups.put(name, group);
        return group;
    }

    @Override
    public Group makeGroup(String name, Channel target) {
        GroupImpl group = new GroupImpl(name, target);
        group.init();
        channelGroups.get(target).put(name, group);
        return group;
    }

    @Override
    public void removeGroup(String name) {
        GroupImpl group = (GroupImpl) groups.get(name);
        group.trash();
    }

    @Override
    public void removeGroup(String name, Channel channel) {
    }

    private class GroupImpl implements Group {

        public GroupImpl(String name) {

        }
        public GroupImpl(String name, Channel channel) {

        }

        @Override
        public Boolean hasPermission(String permission) {
            return null;
        }

        @Override
        public Group getParent() {
            return null;
        }

        @Override
        public void setParent(Group group) {
        }

        @Override
        public void setPermission(String permission, Boolean value) {
        }

        public void init() {
        }

        public void trash() {

        }
    }
}
