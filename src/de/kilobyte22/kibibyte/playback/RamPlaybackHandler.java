package de.kilobyte22.kibibyte.playback;

import de.kilobyte22.kibibyte.api.bufferplayback.Message;
import de.kilobyte22.kibibyte.api.bufferplayback.PlaybackHandler;
import org.pircbotx.Channel;

import java.util.*;

public class RamPlaybackHandler implements PlaybackHandler {

    private Map<Channel, List<Message>> playback = new HashMap<>();

    @Override
    public List<Message> getMessages(Channel channel, int start, int count) {
        List<Message> ret = new LinkedList<>();
        List<Message> pb = playback.get(channel);
        int dir = start < 0 ? -1 : 1;
        // make positive
        start = start * dir - 1;
        if (start >= pb.size()) return ret;
        count = count + start >= pb.size() ? pb.size() - start : count;
        for (int i = start; i < count; i += dir) {
            ret.add(pb.get(i));
        }
        return ret;
    }

    @Override
    public Iterator<Message> getMessageIterator(final Channel channel, final int start) {
        return new Iterator<Message>() {
            int current = start;
            int dir = 1;
            final List<Message> pb = new LinkedList<>(playback.get(channel));
            final int size = pb.size();
            {
                if (current < 0) {
                    dir = -1;
                    current = size - current - 2;
                } else
                current--;
            }
            @Override
            public boolean hasNext() {
                return dir > 0 ? size > current : current >= 0;
            }

            @Override
            public Message next() {
                Message m = pb.get(current);
                current += dir;
                return m;
            }

            @Override
            public void remove() {
                throw new UnsupportedOperationException("No deletion with this one :P");
            }
        };
    }

    @Override
    public void appendMessage(Channel channel, Message message) throws IllegalArgumentException {
        List<Message> pb = playback.get(channel);
        pb.add(message);
        if (pb.size() > 100)
            pb.remove(0);
    }

    @Override
    public String getName() {
        return "ram";
    }

    @Override
    public boolean isPersistant() {
        return false;
    }

    @Override
    public void setupChannel(Channel channel) {
        playback.put(channel, new LinkedList<Message>());
    }

    @Override
    public void cleanupChannel(Channel channel) {
        playback.remove(channel);
    }
}
