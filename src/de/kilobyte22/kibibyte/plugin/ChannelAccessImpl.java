package de.kilobyte22.kibibyte.plugin;

import de.kilobyte22.kibibyte.CommandManagerImpl;
import de.kilobyte22.kibibyte.api.command.CommandManager;
import de.kilobyte22.kibibyte.api.command.PluginCommandManager;
import de.kilobyte22.kibibyte.api.core.ChannelAccess;
import de.kilobyte22.kibibyte.api.core.Kibibyte;
import de.kilobyte22.kibibyte.api.event.EventBus;
import org.pircbotx.Channel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ChannelAccessImpl implements ChannelAccess {

    private final Logger logger;
    private final Channel channel;
    private final ChannelEventBus eventBus;
    private final PluginCommandManager mgr;

    public ChannelAccessImpl(Channel channel, String name) {
        logger = LoggerFactory.getLogger("PLUGIN/" + channel.getName() + "/" + name);
        this.channel = channel;
        eventBus = new ChannelEventBus(channel);
        mgr = new CommandManagerImpl();
    }

    @Override
    public Channel getChannel() {
        return channel;
    }

    @Override
    public Kibibyte getBot() {
        return (Kibibyte) channel.getBot();
    }

    @Override
    public EventBus getEventBus() {
        return eventBus;
    }

    @Override
    public Logger getLogger() {
        return logger;
    }

    @Override
    public PluginCommandManager getCommandManager() {
        return mgr;
    }
}
