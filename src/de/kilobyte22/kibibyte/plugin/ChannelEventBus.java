package de.kilobyte22.kibibyte.plugin;

import de.kilobyte22.kibibyte.core.Kibibyte;
import de.kilobyte22.kibibyte.event.EventBus;
import org.pircbotx.Channel;
import org.pircbotx.User;

public class ChannelEventBus extends EventBus {

    private final Channel channel;

    public ChannelEventBus(Channel channel) {

        this.channel = channel;
    }

    @Override
    protected boolean passesCustomFilter(HandlerInfo info, Object event, Kibibyte bot, Channel channel, User user) {
        return channel == null || channel == this.channel;
    }
}
