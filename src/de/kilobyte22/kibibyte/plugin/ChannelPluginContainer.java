package de.kilobyte22.kibibyte.plugin;

import de.kilobyte22.kibibyte.api.core.ChannelAccess;
import de.kilobyte22.kibibyte.api.plugin.ChannelPlugin;

public class ChannelPluginContainer extends PluginContainer {

    private final ChannelPlugin plugin;
    private final ChannelAccess access;

    public ChannelPluginContainer(ChannelPlugin plugin, ChannelAccess access) {
        this.plugin = plugin;
        this.access = access;
    }

    @Override
    public void disable() {
        plugin.unload();
    }

    @Override
    public ChannelPlugin getPluginInstance() {
        return plugin;
    }

    @Override
    public ChannelAccess getBotAccess() {
        return access;
    }
}
