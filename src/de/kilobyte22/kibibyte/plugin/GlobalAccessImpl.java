package de.kilobyte22.kibibyte.plugin;

import de.kilobyte22.kibibyte.CommandManagerImpl;
import de.kilobyte22.kibibyte.api.command.CommandManager;
import de.kilobyte22.kibibyte.api.command.PluginCommandManager;
import de.kilobyte22.kibibyte.api.core.GlobalAccess;
import de.kilobyte22.kibibyte.api.event.EventBus;
import de.kilobyte22.kibibyte.api.plugin.*;
import de.kilobyte22.kibibyte.api.plugin.PluginSystem;
import de.kilobyte22.kibibyte.api.registry.CommandRegistry;
import de.kilobyte22.kibibyte.core.Botmanager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class GlobalAccessImpl implements GlobalAccess {

    private final Logger logger;
    private PluginCommandManager commandManager;

    public GlobalAccessImpl(String name) {
        logger = LoggerFactory.getLogger("PLUGIN/" + name);
        commandManager = new CommandManagerImpl();
        CommandRegistry.registerManager(commandManager);
    }

    @Override
    public PluginSystem getPluginSystem() {
        return Botmanager.pluginSystem;
    }

    @Override
    public EventBus getEventBus() {
        return Botmanager.eventBus;
    }

    @Override
    public Logger getLogger() {
        return logger;
    }

    @Override
    public PluginCommandManager getCommandManager() {
        return commandManager;
    }

}
