package de.kilobyte22.kibibyte.plugin;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.net.URL;
import java.net.URLClassLoader;
import java.util.HashMap;
import java.util.Map;

public class PluginClassloader {

    private static final Logger logger = LoggerFactory.getLogger("PLUGINLOADER");
    private Map<String, InnerClassLoader> loaders = new HashMap<>();

    public ClassLoader makeClassloader(String name, URL file) {
        logger.debug("Creating loader {}", name);
        InnerClassLoader classLoader = new InnerClassLoader(name, file);
        loaders.put(name, classLoader);
        return classLoader;
    }

    public void disposeClassloader(String name) {
        loaders.remove(name);
    }

    private class InnerClassLoader extends URLClassLoader {
        private final String name;

        public InnerClassLoader(String name, URL file) {
            super(new URL[] {file});
            this.name = name;
        }

        public Class<?> loadClassInternal(String name, boolean resolve) throws ClassNotFoundException {
            return super.loadClass(name, resolve);
        }

        @Override
        protected Class<?> loadClass(String name, boolean resolve) throws java.lang.ClassNotFoundException {
            try {
                return loadClassInternal(name, resolve);
            } catch (Exception ignored) {}
            for (InnerClassLoader loader : loaders.values()) {
                if (loader == this) continue;
                try {
                    return loader.loadClassInternal(name, resolve);
                } catch (Exception ignored) {}
            }
            return Class.forName(name);
        }

        @Override
        public void finalize() throws Throwable {
            logger.debug("Loader {} is about to get eaten by gc :D", name);
            super.finalize();
        }
    }
}
