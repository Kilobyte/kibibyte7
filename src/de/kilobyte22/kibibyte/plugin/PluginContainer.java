package de.kilobyte22.kibibyte.plugin;

import de.kilobyte22.kibibyte.api.core.BotAccess;
import de.kilobyte22.kibibyte.api.plugin.Plugin;

public abstract class PluginContainer {
    public PluginInfo pluginInfo;
    public abstract void disable();
    public abstract Plugin getPluginInstance();
    public abstract BotAccess getBotAccess();
    @Override
    public void finalize() throws Throwable {
        disable();
        super.finalize();
    }
}
