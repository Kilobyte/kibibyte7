package de.kilobyte22.kibibyte.plugin;

import de.kilobyte22.kibibyte.api.plugin.*;
import de.kilobyte22.kibibyte.api.plugin.PluginSystem;

public class PluginInfo {
    public Class pluginClass;
    public PluginSystem.PluginType pluginType;
    public ClassLoader pluginLoader;
    public GlobalPlugin pluginInstance;
    public StaticHandlerClass staticHandler;
}
