package de.kilobyte22.kibibyte.plugin;

import com.google.common.eventbus.Subscribe;
import de.kilobyte22.kibibyte.api.configuration.ConfigFile;
import de.kilobyte22.kibibyte.api.configuration.ConfigList;
import de.kilobyte22.kibibyte.api.configuration.ConfigMap;
import de.kilobyte22.kibibyte.api.configuration.ConfigNode;
import de.kilobyte22.kibibyte.api.core.ChannelAccess;
import de.kilobyte22.kibibyte.api.core.Kibibyte;
import de.kilobyte22.kibibyte.api.event.EventBus;
import de.kilobyte22.kibibyte.api.event.ShutdownEvent;
import de.kilobyte22.kibibyte.api.exception.PluginLoadingException;
import de.kilobyte22.kibibyte.api.plugin.ChannelPlugin;
import de.kilobyte22.kibibyte.api.plugin.GlobalPlugin;
import de.kilobyte22.kibibyte.api.plugin.NetworkPlugin;
import de.kilobyte22.kibibyte.api.plugin.StaticHandler;
import de.kilobyte22.kibibyte.api.registry.CommandRegistry;
import de.kilobyte22.kibibyte.core.Botmanager;
import org.pircbotx.Channel;
import org.pircbotx.hooks.events.JoinEvent;
import org.pircbotx.hooks.events.KickEvent;
import org.pircbotx.hooks.events.PartEvent;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.util.*;
import java.util.jar.JarFile;

public class PluginSystem implements de.kilobyte22.kibibyte.api.plugin.PluginSystem {

    private Map<String, PluginInfo> availablePlugins = new HashMap<>();
    private Map<String, File> allPlugins = new HashMap<>();
    private PluginClassloader loader = new PluginClassloader();
    private static final Logger logger = LoggerFactory.getLogger("PLUGINSYS");

    private Map<Kibibyte, Map<String, ServerPluginContainer>> serverPlugins = new WeakHashMap<>();
    private Map<Channel, Map<String, ChannelPluginContainer>> channelPlugins = new WeakHashMap<>();
    private List<de.kilobyte22.kibibyte.api.event.EventBus> eventHandlers = new LinkedList<>();

    public PluginSystem() {
        Botmanager.eventBus.register(this);
        File pfile = new File("plugins");
        if (!pfile.exists()) pfile.mkdir();
        for (File file : pfile.listFiles()) {
            try {
                if (file.isFile()) {
                    JarFile jarFile = new JarFile(file);
                    ConfigFile pluginYml = new ConfigFile();
                    pluginYml.load(jarFile.getInputStream(jarFile.getEntry("kibiplugin.yml")));
                    if (pluginYml.containsKey("name"))
                        allPlugins.put(pluginYml.getString("name"), file);
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    @SuppressWarnings("unchecked")
    @Override
    public void loadPlugin(String name) throws IOException, ClassNotFoundException, IllegalAccessException, InstantiationException {
        if (name.contains("@")) {
            String[] a = name.split("@");
            loadPlugin(a[0], a[1]);
            return;
        }
        File file = allPlugins.get(name);
        JarFile jarFile = new JarFile(file);
        PluginInfo info = new PluginInfo();
        try {
            info.pluginLoader = loader.makeClassloader(name, file.toURI().toURL());
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }


        ConfigFile pluginYml = new ConfigFile();
        pluginYml.load(jarFile.getInputStream(jarFile.getEntry("kibiplugin.yml")));

        String mainclass = pluginYml.getString("mainclass");
        info.pluginClass = Class.forName(mainclass, true, info.pluginLoader);

        if (info.pluginClass.isAnnotationPresent(StaticHandler.class)) {
            StaticHandler staticHandler = (StaticHandler) info.pluginClass.getAnnotation(StaticHandler.class);
            info.staticHandler = staticHandler.value().newInstance();
            info.staticHandler.setData(new ConfigFile(new File("plugins/" + name + ".cfg")), LoggerFactory.getLogger("plugin/" + name + "/static"));
            info.staticHandler.init();
        }

        if (GlobalPlugin.class.isAssignableFrom(info.pluginClass)) {
            info.pluginType = PluginType.GLOBAL;
            info.pluginInstance = (GlobalPlugin) info.pluginClass.newInstance();
            info.pluginInstance.load(new GlobalAccessImpl(name));
        } else if (NetworkPlugin.class.isAssignableFrom(info.pluginClass)) {
            info.pluginType = PluginType.SERVER;
        } else if (ChannelPlugin.class.isAssignableFrom(info.pluginClass)) {
            info.pluginType = PluginType.CHANNEL;
        } else {
            throw new RuntimeException("Invalid Plugin Class");
        }
        availablePlugins.put(name, info);
    }

    @Override
    public void loadPlugin(String name, String clazz) throws ClassNotFoundException, IllegalAccessException, InstantiationException {

        PluginInfo info = new PluginInfo();

        info.pluginClass = Class.forName(clazz);
        if (info.pluginClass.isAnnotationPresent(StaticHandler.class)) {
            StaticHandler staticHandler = (StaticHandler) info.pluginClass.getAnnotation(StaticHandler.class);
            info.staticHandler = staticHandler.value().newInstance();
            info.staticHandler.setData(new ConfigFile(new File("plugins/" + name + ".cfg")), LoggerFactory.getLogger("plugin/" + name + "/static"));
            info.staticHandler.init();
        }
        if (GlobalPlugin.class.isAssignableFrom(info.pluginClass)) {
            info.pluginType = PluginType.GLOBAL;
            info.pluginInstance = (GlobalPlugin) info.pluginClass.newInstance();
            info.pluginInstance.load(new GlobalAccessImpl(name));
        } else if (NetworkPlugin.class.isAssignableFrom(info.pluginClass)) {
            info.pluginType = PluginType.SERVER;
        } else if (ChannelPlugin.class.isAssignableFrom(info.pluginClass)) {
            info.pluginType = PluginType.CHANNEL;
        } else {
            throw new RuntimeException("Invalid Plugin Class");
        }
        availablePlugins.put(name, info);
    }


    @Override
    public void enablePlugin(String name, Kibibyte kibibyte) throws PluginLoadingException {
        if (!availablePlugins.containsKey(name))
            throw new PluginLoadingException("Unknown Plugin");
        PluginInfo info = availablePlugins.get(name);
        if (info.pluginType != PluginType.SERVER)
            throw new PluginLoadingException("Wrong plugin type");
        if (!serverPlugins.containsKey(kibibyte))
            serverPlugins.put(kibibyte, new HashMap<String, ServerPluginContainer>());
        Map<String, ServerPluginContainer> plugins = serverPlugins.get(kibibyte);
        if (plugins.containsKey(name))
            throw new PluginLoadingException("Already enabled");
        try {
            ServerPluginContainer container = new ServerPluginContainer((NetworkPlugin) info.pluginClass.newInstance());
            container.getPluginInstance().load(kibibyte);
            plugins.put(name, container);
        } catch (InstantiationException | IllegalAccessException e) {
            logger.error("Plugin loading error", e);
            throw new PluginLoadingException("Plugin loading error, please inform bot admin");
        }
    }

    @Override
    public PluginType getPluginType(String name) {
        if (!availablePlugins.containsKey(name))
            return null;
        return availablePlugins.get(name).pluginType;
    }

    @Override
    public boolean pluginExists(String name) {
        return availablePlugins.containsKey(name);
    }

    @Override
    public boolean pluginLoadable(String name) {
        return allPlugins.containsKey(name);
    }

    @Override
    public void enablePlugin(String name, Channel channel) throws PluginLoadingException {
        if (!availablePlugins.containsKey(name))
            throw new PluginLoadingException("Unknown Plugin");
        PluginInfo info = availablePlugins.get(name);
        if (info.pluginType != PluginType.CHANNEL)
            throw new PluginLoadingException("Wrong plugin type");
        if (!channelPlugins.containsKey(channel))
            channelPlugins.put(channel, new HashMap<String, ChannelPluginContainer>());
        Map<String, ChannelPluginContainer> plugins = channelPlugins.get(channel);
        if (plugins.containsKey(name))
            throw new PluginLoadingException("Already enabled");
        try {
            ChannelAccess access = new ChannelAccessImpl(channel, name);
            ChannelPluginContainer container = new ChannelPluginContainer((ChannelPlugin) info.pluginClass.newInstance(), access);
            eventHandlers.add(access.getEventBus());
            container.getPluginInstance().load(access);
            plugins.put(name, container);
            CommandRegistry.registerManager(container.getBotAccess().getCommandManager(), channel);
            // Autoload it
            ConfigMap channelCfg = Botmanager.config.getMap("servers").getMap(channel.getBot().toString()).getMap("channels").getMap(channel.getName());
            ConfigList pluginlist = channelCfg.getList("plugins");
            if (!pluginlist.containsObject(name)) {
                pluginlist.add(name);
            }
            Botmanager.config.save();
        } catch (InstantiationException | IllegalAccessException e) {
            logger.error("Plugin loading error", e);
            throw new PluginLoadingException("Plugin loading error, please inform bot admin");
        }
    }

    @Override
    public void unloadPlugin(String name) {
    }

    @Override
    public void disablePlugin(String name, Kibibyte kibibyte) {
    }

    @Override
    public boolean disablePlugin(String name, Channel channel, boolean permanent) {
        if (!channelPlugins.containsKey(channel) || !channelPlugins.get(channel).containsKey(name))
            return false;
        Map<String, ChannelPluginContainer> cp = channelPlugins.get(channel);
        ChannelPluginContainer container = cp.get(name);
        container.disable();
        eventHandlers.remove(container.getBotAccess().getEventBus());
        CommandRegistry.removeManager(container.getBotAccess().getCommandManager(), channel);
        try {
            if (permanent)
                container.getPluginInstance().cleanup();
        } catch (Exception e) {logger.error("Cought exception from plugin during cleanup", e);}
        cp.remove(name);
        if (cp.size() == 0)
            channelPlugins.remove(channel);

        if (!permanent) return true;

        ConfigMap channelCfg = Botmanager.config.getMap("servers").getMap(channel.getBot().toString()).getMap("channels").getMap(channel.getName());
        ConfigList pluginlist = channelCfg.getList("plugins");
        pluginlist.removeObject(name);
        Botmanager.config.save();
        return true;
    }

    //TODO: use user:self filter
    @Subscribe
    public void onPart(PartEvent event) {
        if (event.getUser() == event.getBot().getUserBot() && channelPlugins.containsKey(event.getChannel())) {
            Channel channel = event.getChannel();
            for (ChannelPluginContainer container : channelPlugins.get(channel).values()) {
                container.disable();
                eventHandlers.remove(container.getBotAccess().getEventBus());
            }
            channelPlugins.remove(channel);
            Botmanager.config.save();
        }
    }

    @Subscribe
    public void onKick(KickEvent event) {
        if (event.getRecipient() == event.getBot().getUserBot() && channelPlugins.containsKey(event.getChannel())) {
            Channel channel = event.getChannel();
            for (ChannelPluginContainer container : channelPlugins.get(channel).values()) {
                container.disable();
                eventHandlers.remove(container.getBotAccess().getEventBus());
            }
            channelPlugins.remove(channel);
            Botmanager.config.save();
        }
    }

    @Subscribe
    public void onJoin(JoinEvent event) {
        if (event.getUser() == event.getBot().getUserBot()) {
            ConfigMap channel = Botmanager.config.getMap("servers").getMap(event.getBot().toString()).getMap("channels").getMap(event.getChannel().getName());

            for (ConfigNode node : channel.getList("plugins")) {
                try {
                    enablePlugin(node.toString().split("@")[0], event.getChannel());
                } catch (PluginLoadingException e) {}
            }
            Botmanager.config.save();
        }
    }

    @Subscribe
    public void onEvent(Object event) {
        for (EventBus eventBus : eventHandlers) {
            eventBus.post(event);
        }
    }

    @Subscribe
    public void onShutdown(ShutdownEvent event) {
        for (Map<String, ChannelPluginContainer> p : channelPlugins.values()) {
            for (ChannelPluginContainer c : p.values()) {
                c.disable();
            }
        }
        for (PluginInfo info : availablePlugins.values()) {
            if (info.pluginType == PluginType.GLOBAL)
                info.pluginInstance.unload();
        }
    }
}
