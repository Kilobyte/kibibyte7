package de.kilobyte22.kibibyte.plugin;

import de.kilobyte22.kibibyte.api.core.BotAccess;
import de.kilobyte22.kibibyte.api.plugin.NetworkPlugin;
import de.kilobyte22.kibibyte.api.plugin.Plugin;

public class ServerPluginContainer extends PluginContainer {

    private final NetworkPlugin plugin;

    public ServerPluginContainer(NetworkPlugin plugin) {
        this.plugin = plugin;
    }

    @Override
    public void disable() {
        plugin.unload();
    }

    @Override
    public NetworkPlugin getPluginInstance() {
        return plugin;
    }

    @Override
    public BotAccess getBotAccess() {
        return null;
    }
}
