package de.kilobyte22.kibibyte.sender;

import de.kilobyte22.kibibyte.api.CommandSender;
import de.kilobyte22.kibibyte.api.IrcColor;
import de.kilobyte22.kibibyte.api.core.Kibibyte;
import org.pircbotx.Channel;
import org.pircbotx.User;

public class ChannelCommandSender implements CommandSender {
    private final User user;
    private final Channel channel;
    private final Kibibyte bot;

    public ChannelCommandSender(User user, Channel channel, Kibibyte bot) {
        this.user = user;
        this.channel = channel;
        this.bot = bot;
    }

    @Override
    public void sendMessage(String message, String... params) {
        bot.doDefaultMessage(user, String.format(message, (Object[]) params)); // cast to satisfy compiler
    }

    @Override
    public String getAccount() {
        return bot.getAccount(user);
    }

    @Override
    public boolean hasPermission(String permission) {
        return bot.hasPermission(user, channel, permission);
    }

    @Override
    public User getUser() {
        return user;
    }

    @Override
    public String getName() {
        return user.getNick();
    }

    @Override
    public void reportError(String message, String... params) {
        bot.doDefaultMessage(user, IrcColor.COLOR.toString() + IrcColor.RED.toString() + String.format(message, (Object[]) params)); // cast to satisfy compiler
    }

    @Override
    public String getLocation() {
        return bot.getDisplayName();
    }

    @Override
    public String networkID() {
        return bot.getId();
    }

    @Override
    public boolean canSee(Channel channel) {
        return hasPermission("admin.core.seeall") || (!channel.isSecret() || user.getChannels().contains(channel));
    }

    @Override
    public String getFullAccount() {
        return getAccount() + "@" + user.getBot().toString();
    }
}
