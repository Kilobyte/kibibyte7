package de.kilobyte22.test;


import de.kilobyte22.kibibyte.misc.TransliterationRegex;

import java.util.Scanner;

public class TransliterationExpanderTest {

    public static void main(String... args) {
        Scanner scanner = new Scanner(System.in);
        System.out.print("Search: ");
        String search = scanner.nextLine();
        System.out.print("Replace: ");
        String replace = scanner.nextLine();
        for(;;) {
            try {
                System.out.print("Data (empty to change pattern): ");
                String data = scanner.nextLine();
                if (data.length() == 0) {
                    System.out.print("Search: ");
                    search = scanner.nextLine();
                    System.out.print("Replace: ");
                    replace = scanner.nextLine();
                    System.out.print("Data: ");
                    data = scanner.nextLine();
                }
                System.out.println( new TransliterationRegex(search, replace).applyTo(data));
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

}
