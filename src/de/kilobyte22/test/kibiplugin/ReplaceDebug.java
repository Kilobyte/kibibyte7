package de.kilobyte22.test.kibiplugin;

import de.kilobyte22.kibibyte.api.command.Command;
import de.kilobyte22.kibibyte.api.command.CommandParams;
import de.kilobyte22.kibibyte.api.exception.CommandException;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

public class ReplaceDebug {

    private final RegexReplace plugin;

    ReplaceDebug(RegexReplace plugin) {

        this.plugin = plugin;
    }

    @Command(name = "replacedump", usage = "", help = "Dumps some regex parsing info to file")
    public void regexdmp(CommandParams params) {
        try {
            if (plugin.debugEx == null) throw new CommandException("No data to dump");
            String name = "regex_" + System.currentTimeMillis() + ".dmp";
            BufferedWriter w = new BufferedWriter(new FileWriter(new File(name)));
            w.write(plugin.debugEx);
            plugin.debugEx = null;
            w.flush();
            w.close();
            params.getSender().sendMessage("Dumped regex debug data as '%s'", name);
        } catch (IOException e) {
            throw new CommandException(e.getMessage());
        }
    }
}
