package de.kilobyte22.test.kibiplugin;

import de.kilobyte22.kibibyte.api.core.GlobalAccess;
import de.kilobyte22.kibibyte.api.plugin.GlobalPlugin;

public class Test implements GlobalPlugin {
    @Override
    public void load(GlobalAccess access) {
        access.getLogger().info("Success!");
    }

    @Override
    public void unload() {
    }
}
