package de.kilobyte22.test.kibiplugin;

import de.kilobyte22.kibibyte.api.core.ChannelAccess;
import de.kilobyte22.kibibyte.api.plugin.ChannelPlugin;
import org.pircbotx.Channel;

public class TestChannel implements ChannelPlugin {

    ChannelAccess access;

    @Override
    public void load(ChannelAccess access) {
        access.getBot().sendMessage(access.getChannel(), "Loaded");
        this.access = access;
    }

    @Override
    public void cleanup() {
        access.getBot().sendMessage(access.getChannel(), "Cleanup");
    }

    @Override
    public void unload() {
        access.getBot().sendMessage(access.getChannel(), "Unloaded");
    }
}
